var data = {
    "DocumentElement": {
        "Info": {
            "ExamID": "exam_AB_0a_2",
            "ExamTitle": "英語Level 0A主修會話AB升級考",
            "ExamType": "ab",
            "ExamClass": "0A",
            "ExamLanguage": "English",
            "ExamTime": "25"
        },
        "Exam": [
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Vocabulary",
                "GroupID": "1",
                "ID": "1",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "A: What's your _____?<br>B: It's 0928364250.",
                "Picture": "",
                "Item_1": "job",
                "Item_2": "name",
                "Item_3": "address",
                "Item_4": "phone number",
                "Answer": "phone number",
                "Explain": "題目翻譯:<br>A: 你的_____是什麼?<br>B: 是0928364250。<br>答案翻譯:<br>(A)工作。<br>(B)名字。<br>(C)地址。<br>(D)電話號碼，符合句意，故選D。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Vocabulary",
                "GroupID": "2",
                "ID": "2",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "Linda doesn't have a boyfriend or a husband. She is _____.",
                "Picture": "",
                "Item_1": "married",
                "Item_2": "single",
                "Item_3": "marital",
                "Item_4": "status",
                "Answer": "single",
                "Explain": "題目翻譯:<br>Linda(女性名)沒有男朋友或是丈夫。她是_____。<br>答案翻譯:<br>(A)已婚的。<br>(B)單身的，符合句意，故選B。<br>(C)婚姻的。<br>(D)狀態。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Vocabulary",
                "GroupID": "3",
                "ID": "3",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "A: What are the _____ of the fruit salad?<br>B: Apples, oranges, and pears.",
                "Picture": "",
                "Item_1": "ingredients",
                "Item_2": "salt",
                "Item_3": "onions",
                "Item_4": "bags",
                "Answer": "ingredients",
                "Explain": "題目翻譯:<br>A:這水果沙拉的_____有什麼?<br>B: 有蘋果、柳橙還有梨子。<br>答案翻譯:<br>(A)原料，符合句意，故選A。<br>(B)鹽。<br>(C)洋蔥。<br>(D)袋子。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Vocabulary",
                "GroupID": "4",
                "ID": "4",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "A: Which _____ do you live in?<br>B: I live in Florida.",
                "Picture": "",
                "Item_1": "city",
                "Item_2": "state",
                "Item_3": "road",
                "Item_4": "address",
                "Answer": "state",
                "Explain": "題目翻譯:<br>A: 你住在哪一個_____ ?<br>B: 我住在佛羅里達(州)。<br>答案翻譯:<br>(A)城市。<br>(B)州，符合句意，故選B。<br>(C)路。<br>(D)地址。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Vocabulary",
                "GroupID": "5",
                "ID": "5",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "I live in Taipei and the _____ is 103.",
                "Picture": "",
                "Item_1": "zip code",
                "Item_2": "address",
                "Item_3": "state",
                "Item_4": "city",
                "Answer": "zip code",
                "Explain": "題目翻譯:<br>我住在台北，_____是103。<br>答案翻譯:<br>(A)郵遞區號，符合句意，故選A。<br>(B)地址。<br>(C)州。<br>(D)城市。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Vocabulary",
                "GroupID": "6",
                "ID": "6",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "A: My birthday is before July. When is it?<br>B: It's in _____.",
                "Picture": "",
                "Item_1": "November",
                "Item_2": "August",
                "Item_3": "September",
                "Item_4": "June",
                "Answer": "June",
                "Explain": "題目翻譯:<br>A:我的生日在七月前面。我的生日是在什麼時候呢?<br>B:是在______。<br>答案翻譯:<br>(A)11月。<br>(B)8月。<br>(C)9月。<br>(D)6月，符合句意，故選D。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Vocabulary",
                "GroupID": "7",
                "ID": "7",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "Maria eats _____ after every meal.",
                "Picture": "",
                "Item_1": "dessert",
                "Item_2": "hungry",
                "Item_3": "thirsty",
                "Item_4": "dress",
                "Answer": "dessert",
                "Explain": "題目翻譯:<br>Maria(女性名)每餐後都吃_____。<br>答案翻譯:<br>(A)甜點，符合句意，故選A。<br>(B)餓的。<br>(C)口渴的。<br>(D)洋裝。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Vocabulary",
                "GroupID": "8",
                "ID": "8",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "It's hot. I'm wearing a t-shirt and _____.",
                "Picture": "",
                "Item_1": "a jacket",
                "Item_2": "a coat",
                "Item_3": "chips",
                "Item_4": "shorts",
                "Answer": "shorts",
                "Explain": "題目翻譯:<br>天氣好熱。我穿著T恤跟_____。<br>答案翻譯:<br>(A)夾克。<br>(B)大衣外套。<br>(C)薯片。<br>(D)短褲，符合句意，故選D。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Vocabulary",
                "GroupID": "9",
                "ID": "9",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "A: How is the _____ in Tokyo?<br>B: It is windy.",
                "Picture": "",
                "Item_1": "foggy",
                "Item_2": "weather",
                "Item_3": "warm",
                "Item_4": "cold",
                "Answer": "weather",
                "Explain": "題目翻譯:<br>A:東京的_____如何?<br>B:風很大。<br>答案翻譯:<br>(A)多霧的。<br>(B)天氣，符合句意，故選B。<br>(C)溫暖的。<br>(D)冷的。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Vocabulary",
                "GroupID": "10",
                "ID": "10",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "A _____ is ten cents.",
                "Picture": "",
                "Item_1": "penny",
                "Item_2": "quarter",
                "Item_3": "nickel",
                "Item_4": "dime",
                "Answer": "dime",
                "Explain": "題目翻譯:<br>_____等於10分錢。<br>答案翻譯:<br>(A)1分硬幣。<br>(B)25分硬幣。<br>(C)5分硬幣。<br>(D)10分硬幣，符合句意，故選D。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Grammar",
                "GroupID": "11",
                "ID": "11",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "It's cold. Kenji, Jenny, and Sean _____ warm clothing.",
                "Picture": "",
                "Item_1": "need",
                "Item_2": "needs",
                "Item_3": "to need",
                "Item_4": "needing",
                "Answer": "need",
                "Explain": "題目翻譯:<br>天氣冷。Kenji(男性名)、Jenny(女性名)跟Sean(男性名)需要保暖衣物。<br>選項解析:<br>現在簡單式句型:複數主詞+動詞原形。主詞共三個人物故為複數。<br>(A)need為動詞原形，故選A。<br>(B)needs為現在簡單式三單動詞變化，故不可選B。<br>(C)to need為不定詞(to+動詞原形)，故不可選C。<br>(D)needing為現在分詞(動詞原形-ing)，故不可選D。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Grammar",
                "GroupID": "12",
                "ID": "12",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "I eat dinner _____ six in the evening.",
                "Picture": "",
                "Item_1": "in",
                "Item_2": "on",
                "Item_3": "at",
                "Item_4": "between",
                "Answer": "at",
                "Explain": "題目翻譯:<br>我在晚間6點吃晚餐。<br>選項解析:<br>(A)in使用在月份、季節、年、一段長而非特定的時間，故不可選A。<br>(B)on使用在特定日期、星期，故不可選B。<br>(C)at使用在一個準確、特定的時間，晚間6點屬於此範疇，故選C。<br>(D)between使用在兩段時間之間，故不可選D。<br>"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Grammar",
                "GroupID": "13",
                "ID": "13",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "A: How much are the pants?<br>B: _____ $32.50.",
                "Picture": "",
                "Item_1": "It is",
                "Item_2": "They are",
                "Item_3": "We are",
                "Item_4": "I am",
                "Answer": "They are",
                "Explain": "題目翻譯:<br>A:這條長褲多少錢?<br>B:它們32塊50分。<br>選項解析:<br>(A)代名詞it使用在單數或不可數物品，故不可選A。<br>(B)代名詞they使用在複數物品，長褲(pants)為複數形，故選B。<br>(C)代名詞we(我們)使用在人，故不可選C。<br>(D)代名詞I(我)使用在人，故不可選D。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Grammar",
                "GroupID": "14",
                "ID": "14",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "My sister _____ chocolate ice cream.",
                "Picture": "",
                "Item_1": "like",
                "Item_2": "likes",
                "Item_3": "to like",
                "Item_4": "liking",
                "Answer": "likes",
                "Explain": "題目翻譯:<br>我妹妹喜歡巧可力冰淇淋。<br>選項解析:<br>現在簡單式句型:三單主詞+動詞原形-s/es。主詞\"我的妹妹\"為三單主詞。<br>(A)like為動詞原形，故不可選A。<br>(B)likes為現在簡單式三單動詞變化，故選B。<br>(C)to like為不定詞(to+動詞原形)，故不可選C。<br>(D)liking為現在分詞(t動詞原形-ing)，故不可選D。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Grammar",
                "GroupID": "15",
                "ID": "15",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "Julie and Ethan _____ Mr. Huang's students.",
                "Picture": "",
                "Item_1": "be",
                "Item_2": "am",
                "Item_3": "is",
                "Item_4": "are",
                "Answer": "are",
                "Explain": "題目翻譯:<br>Julie(女性名)跟Ethan(男性名)是黃老師的學生。<br>選項解析:<br>現在簡單式句型:複數主詞+are。主詞兩個人物故為複數。<br>(A)be為動詞原形，故不可選A。<br>(B)am為第一人稱單數be動詞，故不可選B。<br>(C)is為第三人稱單數be動詞，故不可選C。<br>(D)are為第二人稱單數/複數be動詞，故選D。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Grammar",
                "GroupID": "16",
                "ID": "16",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "This is Mr. Jackson. _____ phone number is 0912654385.",
                "Picture": "",
                "Item_1": "My",
                "Item_2": "His",
                "Item_3": "Her",
                "Item_4": "Your",
                "Answer": "His",
                "Explain": "題目翻譯:<br>這位是Jackson先生。_____電話號碼是0912654385。<br>選項解析:<br>主詞稱謂使用Mr.(先生)=He。<br>(A)My(我的)為第一人稱\"I\"的所有格形容詞，故不可選A。<br>(B)His(他的)為第三人稱男性\"he\"的所有格形容詞，故選B。<br>(C)Her(她的)為第三人稱女性\"she\"的所有格形容詞，故不可選C。<br>(D)Your(你/你們的)為第二人稱\"you\"的所有格形容詞，故不可選D。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Grammar",
                "GroupID": "17",
                "ID": "17",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "Alan and I live together. _____ address is 1625 Main Street.",
                "Picture": "",
                "Item_1": "Our",
                "Item_2": "Your",
                "Item_3": "Their",
                "Item_4": "His",
                "Answer": "Our",
                "Explain": "題目翻譯:<br>Alan(男性名)跟我住一起。_____地址是Main Street(街道名)1625號。<br>選項解析:<br>主詞Alan and I=We。<br>(A)Our(我們的)為第一人稱\"we\"的所有格形容詞，故選A。<br>(B)Your(你/你們的)為第二人稱\"you\"的所有格形容詞，故不可選B。<br>(C)Their(他們的)為第三人稱\"they\"的所有格形容詞，故不可選C。<br>(D)His(他的)為第三人稱男性\"he\"的所有格形容詞，故不可選D。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Grammar",
                "GroupID": "18",
                "ID": "18",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "A: _____sweaters do you want?<br>B: I need three sweaters.",
                "Picture": "",
                "Item_1": "How much",
                "Item_2": "How many",
                "Item_3": "How",
                "Item_4": "Where",
                "Answer": "How many",
                "Explain": "題目翻譯:<br>A:你要_____毛衣?<br>B:我要三件毛衣。<br>選項解析:<br>根據答案，問題應該是詢問數量。<br>(A)How much用於詢問價錢/數量，但若how much詢問數量後須接不可數名詞，sweater為可數名詞，故不可選A。<br>(B)How many用於詢問數量後接可數名詞，sweater為可數名詞，故選B。<br>(C)How用於詢問方法，故不可選C。<br>(D)Where用於詢問地點，故不可選D。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Grammar",
                "GroupID": "19",
                "ID": "19",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "Jason _____ a pair of white shoes.",
                "Picture": "",
                "Item_1": "have",
                "Item_2": "has",
                "Item_3": "to have",
                "Item_4": "having",
                "Answer": "has",
                "Explain": "題目翻譯:<br>Jason(男性名)有一雙白色的鞋子。<br>選項解析:<br>現在簡單式句型:三單主詞+動詞-s/es。Jason為三單主詞。<br>(A)have動詞原形，故不可選A。<br>(B)has為三單動詞變化形，故選B。<br>(C)to have為不定詞(to+動詞原形)，故不可選C。<br>(D)having為現在分詞(動詞-ing)，故不可選D。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Grammar",
                "GroupID": "20",
                "ID": "20",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "I like black. I _____ ten black shirts.",
                "Picture": "",
                "Item_1": "have",
                "Item_2": "has",
                "Item_3": "to have",
                "Item_4": "having",
                "Answer": "have",
                "Explain": "題目翻譯:<br>我喜歡黑色。我有10件黑襯衫。<br>選項解析:<br>現在簡單式句型:第一人稱+動詞原形。I 為第一人稱單數。<br>(A)have為動詞原形，故選A。<br>(B)has為第三人稱單數動詞變化形，故不可選B。<br>(C)to have為不定詞(to+動詞原形)，故不可選C。<br>(D)having為現在分詞(動詞-ing)，故不可選D。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Grammar",
                "GroupID": "21",
                "ID": "21",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "Wendy lives _____ an apartment.",
                "Picture": "",
                "Item_1": "in",
                "Item_2": "on",
                "Item_3": "at",
                "Item_4": "between",
                "Answer": "in",
                "Explain": "題目翻譯:<br>Wendy(女性名)住在出租公寓。<br>選項解析:<br>公寓屬於一個明確的空間。<br>(A)in使用表示在一個空間內，故選A。<br>(B)on使用表示在一個面之上，故不可選B。<br>(C)at使用表示在一個點的位置，故不可選C。<br>(D)between使用表示在兩點之間的位置，故不可選D。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Grammar",
                "GroupID": "22",
                "ID": "22",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "Cindy and Mandy live _____ Park Road.",
                "Picture": "",
                "Item_1": "in",
                "Item_2": "on",
                "Item_3": "at",
                "Item_4": "between",
                "Answer": "on",
                "Explain": "題目翻譯:<br>Cindy(女性名)跟Mandy(女性名)住在Park Road(路名)。<br>選項解析:<br>道路屬於一個平面。<br>(A)in使用表示在一個空間內，故不可選A。<br>(B)on使用表示在一個面之上，故選B。<br>(C)at使用表示在一個點的位置，故不可選C。<br>(D)between使用表示在兩點之間的位置，故不可選D。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Grammar",
                "GroupID": "23",
                "ID": "23",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "A: _____ English class?<br>B: It's at 10:30AM.",
                "Picture": "",
                "Item_1": "What's",
                "Item_2": "How's",
                "Item_3": "When's",
                "Item_4": "Where's",
                "Answer": "When's",
                "Explain": "題目翻譯:<br>A:英文課是_____?<br>B:它在早上10點30分。<br>選項解析:<br>根據答案10點30分，得知問題是詢問時間。<br>(A)What用於詢問特定資訊，故不可選A。<br>(B)How用於詢問方法，故不可選B。<br>(C)When用於詢問時間，故選C。<br>(D)Where用於詢問地點，故不可選D。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Grammar",
                "GroupID": "24",
                "ID": "24",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "A: What time is it now?<br>B: It's _____ 8:00.",
                "Picture": "",
                "Item_1": "in",
                "Item_2": "on",
                "Item_3": "at",
                "Item_4": "x",
                "Answer": "x",
                "Explain": "題目翻譯:<br>A:現在幾點?<br>B:現在8點。<br>選項解析:<br>回答幾點幾分，前面不可加介係詞。<br>(A)故不可選A。<br>(B)故不可選B。<br>(C)故不可選C。<br>(D)故選D。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Grammar",
                "GroupID": "25",
                "ID": "25",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "A: How many pairs of shorts are there?<br>B: _____ one pair of shorts.",
                "Picture": "",
                "Item_1": "There is",
                "Item_2": "There are",
                "Item_3": "They are",
                "Item_4": "It is",
                "Answer": "There is",
                "Explain": "題目翻譯:<br>A:有幾條短褲?<br>B:有一條短褲。<br>選項解析:<br>量詞one pair of為單數。<br>表示在某處有句型:There is+單數名詞。<br>(A)故選A。<br>(B)there are後需接複數名詞，故不可選B。<br>(C)問題問數量，需用there is/are句型，故不可選C。<br>(D)問題問數量，需用there is/are句型，故不可選D。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Grammar",
                "GroupID": "26",
                "ID": "26",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "A: How much _____ the blouse?<br>B: $20.",
                "Picture": "",
                "Item_1": "be",
                "Item_2": "am",
                "Item_3": "is",
                "Item_4": "are",
                "Answer": "is",
                "Explain": "題目翻譯:<br>A:這件女性短上衣多少錢?<br>B:20元。<br>選項解析:<br>the blouse為單數名詞。<br>How much+is+單數名詞?<br>(A)故不可選A。<br>(B)故不可選B。<br>(C)故選C。<br>(D)故不可選D。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Grammar",
                "GroupID": "27",
                "ID": "27",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "Tina _____ a sandwich and an apple for breakfast.",
                "Picture": "",
                "Item_1": "eat",
                "Item_2": "eats",
                "Item_3": "to eat",
                "Item_4": "eating",
                "Answer": "eats",
                "Explain": "題目翻譯:<br>Tina(女性名)吃三明治跟蘋果當早餐。<br>選項解析:<br>現在簡單式句型:第三人稱單數+動詞-s/es。Tina為第三人單數主詞。<br>(A)eat為動詞原形，故不可選A。<br>(B)eats為三單變化形動詞，故選B。<br>(C)to eat為不定詞(to+動詞原形)，故不可選C。<br>(D)eating為現在分詞(動詞-ing)，故不可選D。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Grammar",
                "GroupID": "28",
                "ID": "28",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "A: Who is talking in the classroom?<br>B: Julie and Jane _____ in the classroom.",
                "Picture": "",
                "Item_1": "be talking",
                "Item_2": "am talking",
                "Item_3": "is talking",
                "Item_4": "are talking",
                "Answer": "are talking",
                "Explain": "題目翻譯:<br>A:誰在教室內聊天?<br>B:Julie(女性名)跟Jane(女性名)在教室內聊天。<br>選項解析:<br>Julie and Jane為兩個人物故為複數。<br>現在進行式句型:複數主詞+are+現在分詞(動詞-ing)。<br>(A)be為動詞原形，故不可選A。<br>(B)am為第一人稱單數動詞，故不可選B。<br>(C)is為第三人稱單數動詞，故不可選C。<br>(D)are為第二人稱單數/複數動詞，故選D。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Grammar",
                "GroupID": "29",
                "ID": "29",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "Martin's brothers _____ burgers and fries for dinner.",
                "Picture": "",
                "Item_1": "want",
                "Item_2": "wants",
                "Item_3": "to want",
                "Item_4": "wanting",
                "Answer": "want",
                "Explain": "題目翻譯:<br>Martin(男性名)的兄弟們想要漢堡跟薯條當晚餐。<br>選項解析:<br>Martin的兄弟們為複數主詞。<br>現在簡單式句型:複數主詞+動詞原形。<br>(A)want為動詞原形，故選A。<br>(B)wants為三單動詞變化，故不可選B。<br>(C)to want為不定詞(to+動詞原形)，故不可選C。<br>(D)wanting為現在分詞(動詞-ing)，故不可選D。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Grammar",
                "GroupID": "30",
                "ID": "30",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "A: What does Women's section have?<br>B: It _____ shoes, blouses, and dresses.",
                "Picture": "",
                "Item_1": "have",
                "Item_2": "has",
                "Item_3": "to have",
                "Item_4": "having",
                "Answer": "has",
                "Explain": "題目翻譯:<br>A:女性服飾區有什麼?<br>B:它有鞋子、短上衣還有洋裝。<br>選項解析:<br>It為三單主詞。<br>現在簡單式句型:三單主詞+動詞-s/es。<br>(A)have為動詞原形，故不可選A。<br>(B)has為三單動詞變化形，故選B。<br>(C)to have為不定詞(to+動詞原形)，故不可選C。<br>(D)having為現在分詞(動詞-ing)，故不可選D。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Comprehension",
                "GroupID": "31",
                "ID": "31",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "There is a lot of food and drink in the kitchen. There is milk and water in the refrigerator. The milk is next to the water. There is turkey, cheese, bread and lettuce on the counter. The lettuce is between the turkey and the bread. The cheese is next to the turkey. There are some oranges over the counter. ",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "Where is the turkey?",
                "Picture": "",
                "Item_1": "It's next to the bread.",
                "Item_2": "It's between the cheese and the lettuce.",
                "Item_3": "It's over the counter.",
                "Item_4": "It's next to the milk.",
                "Answer": "It's between the cheese and the lettuce.",
                "Explain": "閱讀翻譯:<br>廚房裡有很多食物跟飲料。冰箱裡有牛奶跟水。牛奶在水旁邊。櫃檯上有火雞肉、乳酪、麵包還有生菜。生菜在火雞肉跟麵包中間。乳酪在火雞肉旁邊。櫃台上方有一些柳橙。<br><br>題目翻譯:<br>火雞肉在哪?<br>選項解析:<br>(A)在麵包旁，但麵包旁邊的是生菜。<br>(B)火雞肉在乳酪跟生菜中間，故選B。<br>(C)在櫃台上方，但櫃台上方的是柳橙。<br>(D)在牛奶旁，牛奶旁邊的是水。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Comprehension",
                "GroupID": "31",
                "ID": "32",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "What is next to the lettuce?",
                "Picture": "",
                "Item_1": "Water",
                "Item_2": "Cheese",
                "Item_3": "Turkey",
                "Item_4": "Oranges",
                "Answer": "Turkey",
                "Explain": "題目翻譯:<br>生菜旁邊是什麼?<br>題目翻譯:<br>(A)水在牛奶旁邊。<br>(B)乳酪在火雞肉旁邊。<br>(C)火雞肉在生菜跟乳酪中間，故選C。<br>(D)柳橙在櫃台上方。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Comprehension",
                "GroupID": "32",
                "ID": "33",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "My name is William. Today is Tuesday and it's a great day. It's my birthday. My birthday is June 30. Tomorrow is my best friend, Jerry's birthday. He wants a chocolate cake for his birthday. He doesn't want any presents.<br>",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "What's tomorrow's date?",
                "Picture": "",
                "Item_1": "Wednesday",
                "Item_2": "July 1",
                "Item_3": "Friday",
                "Item_4": "June 30",
                "Answer": "July 1",
                "Explain": "閱讀翻譯:<br>我的名字是William(男性名)。今天是星期二且是個很棒的日子。今天是我的生日。我的生日是6月30日。明天是我好朋友Jerry(男性名)的生日。他生日想要個巧克力蛋糕。他不要任何禮物。<br><br>題目翻譯:<br>明天是幾月幾號?<br>選項解析:<br>(A)星期三。<br>(B)今天是6月30號，所以明天是7月1號，故選B。<br>(C)星期五。<br>(D)6月30日。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "Comprehension",
                "GroupID": "32",
                "ID": "34",
                "Hint": "Choose the best answer.",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "What does Jerry want?",
                "Picture": "",
                "Item_1": "He wants a present.",
                "Item_2": "He wants a chocolate cake.",
                "Item_3": "He wants ice cream for dessert.",
                "Item_4": "He wants oranges after meal.",
                "Answer": "He wants a chocolate cake.",
                "Explain": "題目翻譯:<br>Jerry(男性名)要想什麼?<br>選項解析:<br>(A)他想要禮物，這段文章說他不要任何禮物。<br>(B)他想要巧克力蛋糕，故選B。<br>(C)他想要冰淇淋當甜點，這段文章並沒有說。<br>(D)他餐後想吃柳橙，這段文章並沒有說。"
            }
        ]
    }
}