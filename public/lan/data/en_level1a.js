﻿var data = {
    "DocumentElement": {
	   "Info": {
		  "ExamID": "en_level1a",
		  "ExamTitle": "美語 Level1 主修會話升級測驗",
		  "ExamType": "upgrade",
		  "ExamClass": "1",
		  "ExamLanguage": "English",
		  "ExamTime": "40"
	   },
	   "Exam": [
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Vocabulary",
			 "GroupID": "1",
			 "ID": "1",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "What is John's address?",
			 "Picture": "",
			 "Item_1": "555-3657.",
			 "Item_2": "4435 Main Street.",
			 "Item_3": "92745.",
			 "Item_4": "phone number.",
			 "Answer": "4435 Main Street.",
			 "Explain": "句意:John的地址是?/nA.555-3657。/nB.Main街4435號 。/nC.92745。/nD.電話號碼。/n正確的答案為B。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Vocabulary",
			 "GroupID": "2",
			 "ID": "2",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "Ana's phone number is _____",
			 "Picture": "",
			 "Item_1": "12 Calhoun Circle.",
			 "Item_2": "11011.",
			 "Item_3": "(614)663-4676.",
			 "Item_4": "55-56-432.",
			 "Answer": "(614)663-4676.",
			 "Explain": "句意:Ana的電話號碼是___/nA.Calhoun Circle12號。/nB.11011。/nC.(614)663-4676./nD.55-56-432。/n正確的答案為C。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Grammar",
			 "GroupID": "3",
			 "ID": "3",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "Maria _____ long black hair.",
			 "Picture": "",
			 "Item_1": "have",
			 "Item_2": "does",
			 "Item_3": "is",
			 "Item_4": "has",
			 "Answer": "has",
			 "Explain": "句意:Maria有一頭黑長髮。/n擁有的英文為have，因Maria為第三人稱單數，故have需改為has。正確答案為D。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Vocabulary",
			 "GroupID": "4",
			 "ID": "4",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "Maureen lives at _____.",
			 "Picture": "",
			 "Item_1": "833-7654",
			 "Item_2": "10 Campus St.",
			 "Item_3": "97843-83",
			 "Item_4": "address",
			 "Answer": "10 Campus St.",
			 "Explain": "句意:Maureen住在___。/nA. 833-7654/nB.Campus街10號/nC. 9784-83/nD.地址/n正確的答案為B。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Grammar",
			 "GroupID": "5",
			 "ID": "5",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "You _____ brown hair and green eyes.",
			 "Picture": "",
			 "Item_1": "has",
			 "Item_2": "is",
			 "Item_3": "are",
			 "Item_4": "have",
			 "Answer": "have",
			 "Explain": "句意:你有棕色頭髮和綠色眼睛。/nYou為第二人稱，動詞不需變化。答案為D。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Grammar",
			 "GroupID": "6",
			 "ID": "6",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "He _____ movies.",
			 "Picture": "",
			 "Item_1": "like",
			 "Item_2": "is",
			 "Item_3": "likes",
			 "Item_4": "have",
			 "Answer": "likes",
			 "Explain": "句意:他喜歡電影。/n選項A和C都是喜歡的意思，但因主詞He為第三人稱單數，一般動詞後需加s/es，故正確答案為C。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Vocabulary",
			 "GroupID": "7",
			 "ID": "7",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "Please take out a piece of _____.",
			 "Picture": "",
			 "Item_1": "book",
			 "Item_2": "pen",
			 "Item_3": "pencil",
			 "Item_4": "paper",
			 "Answer": "paper",
			 "Explain": "句意:請拿出一___。/nA.(本)書本 /nB.(枝)筆/nC.(枝)鉛筆 /nD.(張)紙/n正確答案為D。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Vocabulary",
			 "GroupID": "8",
			 "ID": "8",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "Alberto lives in Utah. His zip code is _____.",
			 "Picture": "",
			 "Item_1": "555-3657",
			 "Item_2": "4432 Main Street",
			 "Item_3": "Main Street",
			 "Item_4": "84010",
			 "Answer": "84010",
			 "Explain": "句意:Alberto住在Utah。他的郵遞區號是____。/nA.555-3657/nB.Main街4432號/nC.Main街/nD.84010/n正確答案為D。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Vocabulary",
			 "GroupID": "9",
			 "ID": "9",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "Where do people enter a home?",
			 "Picture": "",
			 "Item_1": "Bathroom.",
			 "Item_2": "Front door.",
			 "Item_3": "Bedroom.",
			 "Item_4": "Dining room.",
			 "Answer": "Front door.",
			 "Explain": "句意:人們從何處進入屋內?/nA.浴室。/nB.前門。/nC.臥室。/nD.餐廳。/n正確答案為B。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Grammar",
			 "GroupID": "10",
			 "ID": "10",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "Gilberto _____ TV.",
			 "Picture": "",
			 "Item_1": "likes",
			 "Item_2": "is",
			 "Item_3": "are",
			 "Item_4": "like",
			 "Answer": "likes",
			 "Explain": "句意:Gilerto喜歡電視。/n選項A和D都是喜歡的意思，主詞Gilerto為第三人稱單數，一般動詞後需加s/es，所以正確答案為A。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Grammar",
			 "GroupID": "11",
			 "ID": "11",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "_____ has blue eyes.",
			 "Picture": "",
			 "Item_1": "They",
			 "Item_2": "We",
			 "Item_3": "She",
			 "Item_4": "Her",
			 "Answer": "She",
			 "Explain": "句意:___有著藍色的眼睛。/n此句動詞為has，因此主詞為第三人稱單數he, she 或it。正確答案為C。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Grammar",
			 "GroupID": "12",
			 "ID": "12",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "John _____ pizza on Saturdays.",
			 "Picture": "",
			 "Item_1": "buy",
			 "Item_2": "eats",
			 "Item_3": "needing",
			 "Item_4": "wanting",
			 "Answer": "eats",
			 "Explain": "句意:John每個星期天都___披薩。/n主詞John為第三人稱單數，一般動詞後需加s/es。正確答案為B。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Grammar",
			 "GroupID": "13",
			 "ID": "13",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "We _____ the kitchen together right now.",
			 "Picture": "",
			 "Item_1": "are cleaning",
			 "Item_2": "is cleaning",
			 "Item_3": "clean",
			 "Item_4": "cleans",
			 "Answer": "are cleaning",
			 "Explain": "句意:我們現在正在打掃廚房。/n句尾right now 顯示本句為現在進行式，句型應為\"主詞+be動詞+Ving\"。又因主詞為複數型，be動詞需用are。正確答案為A。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Grammar",
			 "GroupID": "14",
			 "ID": "14",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "Darla has a headache. She _____ two aspirin.",
			 "Picture": "",
			 "Item_1": "should to take",
			 "Item_2": "should take",
			 "Item_3": "shoulds take",
			 "Item_4": "should takes",
			 "Answer": "should take",
			 "Explain": "句意: Darla頭痛。她應該服用兩顆阿斯匹靈。/n第二句為第一句的建議。情境助動詞(should)後應接動詞原形，正確答案為B。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Vocabulary",
			 "GroupID": "15",
			 "ID": "15",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "Where do people take showers?",
			 "Picture": "",
			 "Item_1": "Hall.",
			 "Item_2": "Living room.",
			 "Item_3": "Bathroom.",
			 "Item_4": "Yard.",
			 "Answer": "Bathroom.",
			 "Explain": "句意:人們在哪洗澡?/nA.大廳。/nB.客廳。 /nC.浴室。/nD.院子。/n正確答案為C。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Grammar",
			 "GroupID": "16",
			 "ID": "16",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "I _____ peanut butter and jelly sandwiches in the morning.",
			 "Picture": "",
			 "Item_1": "eat",
			 "Item_2": "eats",
			 "Item_3": "am eating",
			 "Item_4": "am eat",
			 "Answer": "eat",
			 "Explain": "句意:我早上__花生果醬三明治。/n主詞I(我)為第一人稱單數，使用動詞原形。正確答案為A。選項C為錯誤，因為 I am eating 的時間為right now，題目句尾已有 in the morning，故不可選C。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Grammar",
			 "GroupID": "17",
			 "ID": "17",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "Right now, he _____ in the yard.",
			 "Picture": "",
			 "Item_1": "plays",
			 "Item_2": "is playing",
			 "Item_3": "are playing",
			 "Item_4": "playing",
			 "Answer": "is playing",
			 "Explain": "句意:現在他正在院子裡玩耍。/n從right now可得知本句為現在進行式，句型為\"主詞+be動詞+Ving\"。主詞he為第三人稱單數搭配的be動詞應為is，正確答案為B。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Vocabulary",
			 "GroupID": "18",
			 "ID": "18",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "Please listen _____.",
			 "Picture": "",
			 "Item_1": "down",
			 "Item_2": "book",
			 "Item_3": "carefully",
			 "Item_4": "out",
			 "Answer": "carefully",
			 "Explain": "句意:請___聽。 /nA.不注意。/nB.書本。/nC.仔細地。 /nD.試著。/n正確答案為C。選項D不可選，因為 listen out 後需加 for 某事方為正確。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Grammar",
			 "GroupID": "19",
			 "ID": "19",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "Michelle and Nadia _____ short curly hair.",
			 "Picture": "",
			 "Item_1": "has",
			 "Item_2": "is",
			 "Item_3": "are",
			 "Item_4": "have",
			 "Answer": "have",
			 "Explain": "句意:Michelle和Nadia有著一頭短捲髮。/n擁有的英文為have，主詞Michelle和Nadia為複數故have不需變型，正確答案為D。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Grammar",
			 "GroupID": "20",
			 "ID": "20",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "Maria and John _____ sandwiches every week.",
			 "Picture": "",
			 "Item_1": "make",
			 "Item_2": "cook",
			 "Item_3": "study",
			 "Item_4": "bake",
			 "Answer": "make",
			 "Explain": "句意:Maria和John每週都會做三明治。/n正確的動詞使用為A:「製作」三明治。cook為烹調，通常接需要生火的菜色(糕點除外)，study為「研讀」。Bake為「烘烤」(糕點)。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Grammar",
			 "GroupID": "21",
			 "ID": "21",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "They _____ for help right now.",
			 "Picture": "",
			 "Item_1": "calling",
			 "Item_2": "calls",
			 "Item_3": "call",
			 "Item_4": "are calling",
			 "Answer": "are calling",
			 "Explain": "句意:他們現在正在求救。/n由right now 得知本句為現在進行式，句型為\"主詞+be動詞+Ving\"。主詞They為第三人稱複數搭配的be動詞需用are，正確答案為D。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Vocabulary",
			 "GroupID": "22",
			 "ID": "22",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "Where do pople sit on a sofa?",
			 "Picture": "",
			 "Item_1": "In the hall.",
			 "Item_2": "In the living room.",
			 "Item_3": "In the bathroom.",
			 "Item_4": "In the yard.",
			 "Answer": "In the living room.",
			 "Explain": "句意:人們坐的沙發放哪?/nA.大廳。/nB.客廳。/nC.浴室。/nD.院子。/n合適的答案為B。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Grammar",
			 "GroupID": "23",
			 "ID": "23",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "_____ have straight brown hair.",
			 "Picture": "",
			 "Item_1": "I",
			 "Item_2": "He",
			 "Item_3": "It",
			 "Item_4": "She",
			 "Answer": "I",
			 "Explain": "句意:___有著一頭棕色長髮。/n此句動詞為have，因此可以排除第三人稱單數主詞(He、It、She)。正確答案為A。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Grammar",
			 "GroupID": "24",
			 "ID": "24",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "Right now, you _____ to me.",
			 "Picture": "",
			 "Item_1": "talk",
			 "Item_2": "are talking",
			 "Item_3": "talking",
			 "Item_4": "am talking",
			 "Answer": "are talking",
			 "Explain": "句意:現在你正在和我說話。/n由 right now 可得知本句為現在進行式，句型為\"主詞+be動詞+Ving\"。主詞you為第二人稱，搭配的be動詞應為are。正確答案為B。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Grammar",
			 "GroupID": "25",
			 "ID": "25",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "I have a goal. I _____ every day.",
			 "Picture": "",
			 "Item_1": "wants exercise",
			 "Item_2": "am want to exercise",
			 "Item_3": "wanting exercise",
			 "Item_4": "want to exercise",
			 "Answer": "want to exercise",
			 "Explain": "句意:我有個目標。我要每天運動。/n主詞I為第一人稱單數主要動詞不需變化。表達想做的什麼事，應該使用 want + 不定詞(to+原型動詞)。正確答案為D。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Grammar",
			 "GroupID": "26",
			 "ID": "26",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "Gaspar's baby is sick. He _____ her aspirin.",
			 "Picture": "",
			 "Item_1": "shouldn't give",
			 "Item_2": "shouldn't to give",
			 "Item_3": "shoulds not take",
			 "Item_4": "should not gives",
			 "Answer": "shouldn't give",
			 "Explain": "句意:Gaspar的小孩生病了。他不應該給她服用阿斯匹靈。/n情境助動詞(should)否定句型為\"情境助動詞+not+原形動詞\"。Should not縮寫為shouldn't，正確答案為A。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Grammar",
			 "GroupID": "27",
			 "ID": "27",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "They _____ sports.",
			 "Picture": "",
			 "Item_1": "liking",
			 "Item_2": "like",
			 "Item_3": "likes",
			 "Item_4": "is",
			 "Answer": "like",
			 "Explain": "句意:他們喜歡運動。 /n選項B和C都是喜歡的意思，主詞They為第三人稱複數，一般動詞不需變化，正確答案為B。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Grammar",
			 "GroupID": "28",
			 "ID": "28",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "We _____ basketball.",
			 "Picture": "",
			 "Item_1": "are",
			 "Item_2": "like",
			 "Item_3": "is",
			 "Item_4": "likes",
			 "Answer": "like",
			 "Explain": "句意:我們喜歡籃球。/n選項B和D都是喜歡的意思，主詞We為第一人稱複數，一般動詞不需變化，正確答案為B。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Grammar",
			 "GroupID": "29",
			 "ID": "29",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "_____ like pizza.",
			 "Picture": "",
			 "Item_1": "She",
			 "Item_2": "He",
			 "Item_3": "Rodrigo and Paula",
			 "Item_4": "Maria",
			 "Answer": "Rodrigo and Paula",
			 "Explain": "句意:___喜歡披薩。/n此句動詞為like原形，因此可以排除第三人稱單數主詞(She、He或Maria)，正確答案為C。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Vocabulary",
			 "GroupID": "30",
			 "ID": "30",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "_____ a book.",
			 "Picture": "",
			 "Item_1": "Read",
			 "Item_2": "Listen",
			 "Item_3": "Speak",
			 "Item_4": "Stand up",
			 "Answer": "Read",
			 "Explain": "句意:___一本書。/nA.去閱讀 /nB.去聽 /nC.去說 /nD.站起來/n恰當的動詞只有A。本句為祈使句，說話者叫聽話者去讀本書。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Grammar",
			 "GroupID": "31",
			 "ID": "31",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "Karen is a good swimmer. She _____ in the summer.",
			 "Picture": "",
			 "Item_1": "is wanting swim",
			 "Item_2": "wants swim",
			 "Item_3": "wants to swim",
			 "Item_4": "want swims",
			 "Answer": "wants to swim",
			 "Explain": "句意:Karen游泳游得很好。她想要在夏天游泳。/n主詞She為第三人稱單數，主要動詞需加s/es表達想做的什麼事，應該使用 want + 不定詞(to+原型動詞)。正確答案為C。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Grammar",
			 "GroupID": "32",
			 "ID": "32",
			 "Hint": "選擇最合適的答案。/n",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "I _____ on it right now.",
			 "Picture": "",
			 "Item_1": "is working",
			 "Item_2": "work",
			 "Item_3": "are working",
			 "Item_4": "am working",
			 "Answer": "am working",
			 "Explain": "句意:我現在正在處理中。/n由right now 得知本句應使用現在進行式，句型為\"主詞+be動詞+Ving\"。主詞I為第一人稱單數搭配的be動詞應為am，正確答案為D。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Grammar",
			 "GroupID": "33",
			 "ID": "33",
			 "Hint": "選擇一個恰當的建議。",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "Bang Vu has a sore throat.",
			 "Picture": "",
			 "Item_1": "He should to take aspirin.",
			 "Item_2": "He shoulds take cold medicine.",
			 "Item_3": "He should goes to the doctor.",
			 "Item_4": "He should take throat lozenges.",
			 "Answer": "He should take throat lozenges.",
			 "Explain": "句意:Bang Vu喉嚨痛。/nA.他應該服用阿斯匹靈 。(情境助動詞後只直接接原型動詞，不接to)。/nB.他應該服用感冒藥(情境助動詞本身沒有加s的形式)。/nC.他應該去看醫生(情境助動詞should後只會直接應接原形動詞go而非goes)。/nD.他應該服用喉糖錠。/nA、B與C均文法錯誤。正確答案為D。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Grammar",
			 "GroupID": "34",
			 "ID": "34",
			 "Hint": "選擇一個恰當的建議。",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "Ayumi has a high fever.",
			 "Picture": "",
			 "Item_1": "She shouldn't exercise.",
			 "Item_2": "She shoulds not exercise.",
			 "Item_3": "She shouldn't to exercise.",
			 "Item_4": "She shouldn't exercises.",
			 "Answer": "She shouldn't exercise.",
			 "Explain": "句意:Ayumi發高燒。/nA.她不應該運動。/nB.她不應該運動(情境助動詞本身沒有加s的形式)。/nC.她不應該運動(情境助動詞後只會直接接原形動詞，不接to)。/nD.她不應該運動(情境助動詞後應接原形動詞，exercise不需加s)。/n正確答案為A。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Grammar",
			 "GroupID": "35",
			 "ID": "35",
			 "Hint": "選擇一個恰當的建議。",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "Adam is very tired.",
			 "Picture": "",
			 "Item_1": "He should rest.",
			 "Item_2": "He should take aspirin.",
			 "Item_3": "He shoulds rest.",
			 "Item_4": "He shoulds take aspirin.",
			 "Answer": "He should rest.",
			 "Explain": "句意:Adam非常累。/nA.他應該休息。/nB.他應該服用阿斯匹靈。(疲勞不需要服用阿斯匹靈，建議不正確。)/nC.他應該休息(情境助動詞沒有加s的形式)。/nD.他應該服用阿斯匹靈。(與B同錯，且should 沒有加s的形式。)/n正確答案為A。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Comprehension",
			 "GroupID": "36",
			 "ID": "36",
			 "Hint": "依圖片內容，選擇最合適的答案。",
			 "GroupQuestion": "",
			 "GroupPicture": "en_level1a_36.jpg",
			 "GroupAudio": "",
			 "Question": "What is the total for receipt #3?",
			 "Picture": "",
			 "Item_1": "$1.60.",
			 "Item_2": "$3.20.",
			 "Item_3": "$4.80.",
			 "Item_4": "$5.60.",
			 "Answer": "$4.80.",
			 "Explain": "第三個食譜總共要花多少錢?/nA.$1.60。/nB.$3.20。/nC.$4.80。/nD.$5.60。/n正確答案為C。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Comprehension",
			 "GroupID": "36",
			 "ID": "37",
			 "Hint": "依圖片內容，選擇最合適的答案。",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "What bills and coins do you need for receipt #1?",
			 "Picture": "",
			 "Item_1": "6 one-dollar bills.",
			 "Item_2": "6 one-dollar bills, 4 dimes, and a nickel.",
			 "Item_3": "1 one-dollar bill, 6 five-dollar bills, 2 quarters.",
			 "Item_4": "1 five-dollar bill, 1 one-dollar bill, and 2 quarters.",
			 "Answer": "1 five-dollar bill, 1 one-dollar bill, and 2 quarters.",
			 "Explain": "第一個食譜你需要多少紙鈔和零錢 ?/nA.六張1元紙鈔。/nB.六張1元紙鈔，四個1角硬幣，和一個5分錢硬幣。/nC.一張1元紙鈔，六張5元紙鈔，兩個25分硬幣。/nD.一張5元紙鈔，一張1元紙鈔，和兩個25分硬幣。/n正確答案為D。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Comprehension",
			 "GroupID": "36",
			 "ID": "38",
			 "Hint": "依圖片內容，選擇最合適的答案。",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "What bills and coins do you need for receipt #2?",
			 "Picture": "",
			 "Item_1": "1 five-dollar bill, 1 dime, 1 nickel, and two pennies.",
			 "Item_2": "2 five-dollar bills, 1 dime, and 1 nickel.",
			 "Item_3": "1 ten-dollar bill, 3 nickels, and 2 pennies.",
			 "Item_4": "1 five-dollar bill, 4 one-dollar bills, 1 dime, 1 nickel, and 2 pennies.",
			 "Answer": "1 five-dollar bill, 4 one-dollar bills, 1 dime, 1 nickel, and 2 pennies.",
			 "Explain": "第二個食譜你需要多少紙鈔和零錢 ?/nA.一張5元紙鈔，一個1角硬幣，一個5分錢，和兩個1分硬幣。/nB.兩張5元紙鈔，一個1角硬幣，和一個5分錢。/nC.一張10元紙鈔，三個5分錢硬幣，和兩個1分錢硬幣。/nD.一張5元紙鈔，四張1元紙鈔，一個1角硬幣，一個5分錢硬幣，和兩個1分錢硬幣。/n正確答案為D。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Comprehension",
			 "GroupID": "36",
			 "ID": "39",
			 "Hint": "依圖片內容，選擇最合適的答案。",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "How much is the bread?",
			 "Picture": "",
			 "Item_1": "$1.28.",
			 "Item_2": "$2.48.",
			 "Item_3": "$2.89.",
			 "Item_4": "$3.00.",
			 "Answer": "$3.00.",
			 "Explain": "麵包多少錢?/nA.$1.28。/nB.$2.48。/nC.$2.98。/nD.$3.00。/n正確答案為D。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Comprehension",
			 "GroupID": "36",
			 "ID": "40",
			 "Hint": "依圖片內容，選擇最合適的答案。",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "How much is the cake?",
			 "Picture": "",
			 "Item_1": "$1.60.",
			 "Item_2": "$3.00.",
			 "Item_3": "$3.20.",
			 "Item_4": "$7.08.",
			 "Answer": "$7.08.",
			 "Explain": "蛋糕多少錢?/nA.$1.60。/nB.$3.00。/nC.$3.20。/nD.$7.08。/n正確答案為D。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Comprehension",
			 "GroupID": "37",
			 "ID": "41",
			 "Hint": "依圖片內容，選擇最合適的答案。",
			 "GroupQuestion": "",
			 "GroupPicture": "en_level1a_41.jpg",
			 "GroupAudio": "",
			 "Question": "On clock #1, it is _____.",
			 "Picture": "",
			 "Item_1": "12:00",
			 "Item_2": "12:15",
			 "Item_3": "1:30",
			 "Item_4": "3:00",
			 "Answer": "3:00",
			 "Explain": "第一個時鐘的時間為___。/nA.12:00/nB.12:15/nC.1:30/nD.3:00/n正確答案為D。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Comprehension",
			 "GroupID": "37",
			 "ID": "42",
			 "Hint": "依圖片內容，選擇最合適的答案。",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "On clock #2, it is _____.",
			 "Picture": "",
			 "Item_1": "9:00",
			 "Item_2": "1:30",
			 "Item_3": "2:30",
			 "Item_4": "6:10",
			 "Answer": "1:30",
			 "Explain": "第二個時鐘的時間為___。/nA.9:00/nB.1:30/nC.2:30/nD.6:10/n正確答案為B。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Comprehension",
			 "GroupID": "37",
			 "ID": "43",
			 "Hint": "依圖片內容，選擇最合適的答案。",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "Yoshi reads the newspaper at 1:30 in the afternoon.  Which clock reads 1:30?",
			 "Picture": "",
			 "Item_1": "#1.",
			 "Item_2": "#2.",
			 "Item_3": "#3.",
			 "Item_4": "#4.",
			 "Answer": "#2.",
			 "Explain": "Yoshi下午1點30分看報紙。哪一個時鐘時間為1點30分?/nA.第一個。/nB.第二個。/nC.第三個。 /nD.第四個。/n正確答案為B。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Comprehension",
			 "GroupID": "37",
			 "ID": "44",
			 "Hint": "依圖片內容，選擇最合適的答案。",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "Gabriel listens to the radio at 11:45 in the morning. Which clock reads 11:45?",
			 "Picture": "",
			 "Item_1": "#1.",
			 "Item_2": "#2.",
			 "Item_3": "#3.",
			 "Item_4": "#4.",
			 "Answer": "#3.",
			 "Explain": "Gabriel在上午11點45分聽廣播。哪一個時鐘為11點45分?/nA.第一個。/nB.第二個。/nC.第三個 。/nD.第四個。/n正確答案為C。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Comprehension",
			 "GroupID": "37",
			 "ID": "45",
			 "Hint": "依圖片內容，選擇最合適的答案。",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "Gabriel listens to the radio at 12:15 in the afternoon. Which clock reads 12:15?",
			 "Picture": "",
			 "Item_1": "#1.",
			 "Item_2": "#2.",
			 "Item_3": "#3.",
			 "Item_4": "#4.",
			 "Answer": "#4.",
			 "Explain": "Gabriel在下午12點15分聽廣播。哪一個時鐘為12點15分?/nA.第一個。/nB.第二個。/nC.第三個。 /nD.第四個。/n正確答案為D。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Comprehension",
			 "GroupID": "38",
			 "ID": "46",
			 "Hint": "依圖片內容，選擇最合適的答案。",
			 "GroupQuestion": "",
			 "GroupPicture": "en_level1a_46.jpg",
			 "GroupAudio": "",
			 "Question": "The picture is ____ the wall.",
			 "Picture": "",
			 "Item_1": "under",
			 "Item_2": "next to",
			 "Item_3": "on",
			 "Item_4": "over",
			 "Answer": "on",
			 "Explain": "圖畫在牆的___。/nA.底下。/n0B.旁邊。/nC.上面。/nD.正上方。/n正確答案為C。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Comprehension",
			 "GroupID": "38",
			 "ID": "47",
			 "Hint": "依圖片內容，選擇最合適的答案。",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "The chair is ____ the corner.",
			 "Picture": "",
			 "Item_1": "in",
			 "Item_2": "next to",
			 "Item_3": "on",
			 "Item_4": "over",
			 "Answer": "in",
			 "Explain": "椅子在角落___/nA.裡面。/nB.旁邊。/nC.上面。/nD.正上方。/n正確答案為A(在角落慣用片語為in the cornor)"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Comprehension",
			 "GroupID": "38",
			 "ID": "48",
			 "Hint": "依圖片內容，選擇最合適的答案。",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "The sofa is ____ the end tables.",
			 "Picture": "",
			 "Item_1": "in the corner",
			 "Item_2": "next to",
			 "Item_3": "between",
			 "Item_4": "on",
			 "Answer": "between",
			 "Explain": "沙發在兩張邊桌的___。 /nA.角落/nB.旁邊/nC.兩者之間/nD.上面/n正確答案為C。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Comprehension",
			 "GroupID": "38",
			 "ID": "49",
			 "Hint": "依圖片內容，選擇最合適的答案。",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "The rug is ____ the floor.",
			 "Picture": "",
			 "Item_1": "in",
			 "Item_2": "next to",
			 "Item_3": "between",
			 "Item_4": "on",
			 "Answer": "on",
			 "Explain": "地毯在地板的___。/nA.裡面/nB.旁邊/nC.兩者之間/nD.上面/n正確答案為D。"
		  },
		  {
			 "ModuleName": "ReadingMCQ",
			 "Part": "1",
			 "Type": "Comprehension",
			 "GroupID": "38",
			 "ID": "50",
			 "Hint": "依圖片內容，選擇最合適的答案。",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "",
			 "Question": "The piano is ____ the stereo.",
			 "Picture": "",
			 "Item_1": "between",
			 "Item_2": "next to",
			 "Item_3": "over",
			 "Item_4": "on",
			 "Answer": "next to",
			 "Explain": "鋼琴在音響的___。/nA.兩者之間/nB.旁邊/nC.正上方/nD.上面/n正確答案為B。"
		  },
		  {
			 "ModuleName": "ListeningMCQ",
			 "Part": "2",
			 "Type": "Listening",
			 "GroupID": "39",
			 "ID": "51",
			 "Hint": "依談話內容，選擇最合適的答案。",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "en_level1a_51.mp3",
			 "Question": "What are the directions you hear?",
			 "Picture": "",
			 "Item_1": "Turn around. Turn right on Broadway.",
			 "Item_2": "Turn left. It's on the right off Broadway.",
			 "Item_3": "Turn around. Turn left on Broadway.",
			 "Item_4": "Turn right. It's on the left off Broadway.",
			 "Answer": "Turn around. Turn right on Broadway.",
			 "Explain": "你聽到了哪些方向指示?/nA.迴轉。在Broadway右轉。/nB.左轉。它就在Broadway右邊。/nC.迴轉。在Broadway左轉。/nD.左轉。它就在Broadway左邊。/n正確答案為A。/n/nScript:/nA: Excuse me. I'm looking for the mall. Do you know if it's close by?/nB: Yes, it is. Turn around and then turn right on Broadway. You can't miss it. There are signs all over./nA: OK, turn around and then turn right on Broadway./nB: That's right. It's only a mile away./nA:抱歉。我正在找購物中心。請問你知道它是不是在這附近嗎?/nB:是的，它在附近。迴轉然後在Broadway右轉。你不會錯過它。那邊有很多指示牌。/nA:好的，迴轉然後在Broadway右轉。 /nB:沒錯。它距離只有一公里遠。"
		  },
		  {
			 "ModuleName": "ListeningMCQ",
			 "Part": "2",
			 "Type": "Listening",
			 "GroupID": "40",
			 "ID": "52",
			 "Hint": "依談話內容，選擇最合適的答案。",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "en_level1a_52.mp3",
			 "Question": "What are the directions you hear?",
			 "Picture": "",
			 "Item_1": "Turn right and go one block.",
			 "Item_2": "Turn around and go one block.",
			 "Item_3": "Go straight for about a block.",
			 "Item_4": "Go left and go one block.",
			 "Answer": "Go straight for about a block.",
			 "Explain": "你聽到了哪些方向指示?/nA.向右轉然後走一個街區。/nB.迴轉然後走一個街區。/nC.往前走約一個街區。/nD.向左轉然後走一個街區。/n正確答案為C。/n/nScript:/nA: I'm new here and I need to find the post office to mail a package. Can you give me directions?/nB: Yes, that's easy. Go straight for about a block. It's only a half-mile away on this street./nA: Great. Thank you./nA:我剛搬到這，我需要找到郵局寄包裹。可以請你告訴我怎麼走嗎?/nB:好的，很簡單的。往前走大概一個街區。在這街上約半公里的距離。/nA:棒級了!謝謝你。"
		  },
		  {
			 "ModuleName": "ListeningMCQ",
			 "Part": "2",
			 "Type": "Listening",
			 "GroupID": "41",
			 "ID": "53",
			 "Hint": "依談話內容，選擇最合適的答案。",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "en_level1a_53.mp3",
			 "Question": "What are the directions you hear?",
			 "Picture": "",
			 "Item_1": "Turn left and go to Fairview.",
			 "Item_2": "Turn right and go to Fairview",
			 "Item_3": "Go straight ahead and go to Fairview.",
			 "Item_4": "Turn around and go to Fairview.",
			 "Answer": "Turn around and go to Fairview.",
			 "Explain": "你聽到了哪些方向指示?/nA.左轉然後走到Fairview。/nB.右轉然後走到Favirview。/nC.往前直走然後走到Fairview。/nD.迴轉然後走到Fairview。/n正確答案為D。/n/nScript:/nA: Let's go to the movies./nB: I can't go right now. I'm waiting for a friend to come over./nA: Why don't you and your friend meet us there?/nB: OK. How do you get there?/nA: From here, you turn on Fairview. The Movieplex Theaters are on Fairview, not too far down./nB: Great. We'll see you there. What time?/nA: I think the movie starts at eight./nA:我們去看電影吧!/nB:我現在不能去。我在等一個朋友過來。 /nA:你為什麼不和你朋友一起來戲院和我們碰面?/nB:好。那要怎麼去呢?/nA:從這邊你在Fairview迴轉。Movieplex戲院就在Fairview上，並不會太遠。/nB:好極了!我們那兒見。約幾點呢?/nA:我記得電影晚上8點開演。"
		  },
		  {
			 "ModuleName": "ListeningMCQ",
			 "Part": "2",
			 "Type": "Listening",
			 "GroupID": "42",
			 "ID": "54",
			 "Hint": "依談話內容，選擇最合適的答案。",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "en_level1a_54.mp3",
			 "Question": "What are the directions you hear?",
			 "Picture": "",
			 "Item_1": "Turn right on Center Street. Then turn left of Main.",
			 "Item_2": "Turn left on Center Street. Then turn left on Main.",
			 "Item_3": "Turn around and go to Center Street. Then turn left on Main.",
			 "Item_4": "Go straight ahead on Center Street. Then turn left on Main.",
			 "Answer": "Turn right on Center Street. Then turn left of Main.",
			 "Explain": "你聽到了哪些方向指示?/nA.在Center街上右轉。在Main左轉。/nB.在Center街上左轉。在Main左轉。/nC.迴轉走到Center街。然後在Main左轉。/nD.在Center街上往前直走。然後在Main左轉。/n正確答案為A。/n/nScript:/nA: I'm looking for the Museum of Aviation. Do you know where it is?/nB: Yes. It's a little far. It's downtown on Main./nA: Downtown?/nB: Yes. Turn right on Center Street, then left on Main. Go about five miles on Main. You'll see it close to the police station./nA:我在找航空博物館。你知道在哪嗎?/nB:是的，有一點點遠。它在市中心的Main街上。/nA:市中心?/nB:是的。在Center街右轉，然後在Main街左轉。在Main上走大約5公里。你會看到它在警察局附近。"
		  },
		  {
			 "ModuleName": "ListeningMCQ",
			 "Part": "2",
			 "Type": "Listening",
			 "GroupID": "43",
			 "ID": "55",
			 "Hint": "依談話內容，選擇最合適的答案。",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "en_level1a_55.mp3",
			 "Question": "What is the first direction you hear?",
			 "Picture": "",
			 "Item_1": "Turn left on Broadway.",
			 "Item_2": "Turn left on Main Street.",
			 "Item_3": "Turn left at the next street.",
			 "Item_4": "Turn left after two blocks, then right.",
			 "Answer": "Turn left at the next street.",
			 "Explain": "你聽到的第一個方向指示是什麼?/nA.在Broadway左轉。/nB.在Main街左轉。/nC.在下一條接左轉。/nD.兩個街區後左轉，然後右轉。/n正確答案為C。/n/nScript:/nA: Excuse me. Where is Monterey Park? Do you know?/nB: I think so. It's not a very big park. I think it's in the residential area on Boulder Lane./nA: Oh, now I understand. I was looking on main streets./nB: Turn left at the next street. Then turn right. Go straight ahead for two blocks and you will see it by a school./nA: Thanks. I hope I find it this time./nB: Good luck!/nA:抱歉。Monterey公園在哪?你知道嗎?/nB:我想我應該知道。它不是個大型的公園。我記得它是在住宅區的Boulder巷。/nA:喔!我知道了。我剛都在查看主要道路。/nB:在下條街左轉，然後右轉。往前走兩個街區，然後你會看到它就在學校旁。/nA:謝謝。希望我這次可以找到。/nB:祝你好運!"
		  },
		  {
			 "ModuleName": "ListeningMCQ",
			 "Part": "2",
			 "Type": "Listening",
			 "GroupID": "44",
			 "ID": "56",
			 "Hint": "依談話內容，選擇最合適的答案。",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "en_level1a_56.mp3",
			 "Question": "What is Karen's problem?",
			 "Picture": "",
			 "Item_1": "Her legs hurt, and she can't get to work.",
			 "Item_2": "Her head hurts when she uses the computer.",
			 "Item_3": "Her hand hurts, and she can't use the computer  in the morning.",
			 "Item_4": "Her stomach hurts in the morning.",
			 "Answer": "Her hand hurts, and she can't use the computer  in the morning.",
			 "Explain": "Karen怎麼了?/nA.她的雙腳很痛，導致她無法去上班。/nB.她用電腦的時候頭會痛。/nC.她的手會痛，並且早上的時候無法使用電腦。/nD.她早上會肚子痛。/n正確答案為C。/n/nScript:/nKaren: Doctor, thank you for seeing me on such short notice./nDoctor: What seems to be the trouble?/nKaren: Well, I'm having trouble with my hand./nDoctor: What do you mean, trouble?/nKaren: My hand is very stiff in the morning. I work at a computer and it's getting very difficult to do my work./nKaren:醫生，謝謝你在這麼短的時間內抽空替我看診。/n醫生:有什麼問題呢?/nKaren:嗯，我的手有一些問題。/n醫生:問題?你是指什麼意思?/nKaren:我的手在早上感到很僵硬。我都使用電腦工作所以這樣讓我很難做好我的工作。"
		  },
		  {
			 "ModuleName": "ListeningMCQ",
			 "Part": "2",
			 "Type": "Listening",
			 "GroupID": "45",
			 "ID": "57",
			 "Hint": "依談話內容，選擇最合適的答案。",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "en_level1a_57.mp3",
			 "Question": "What is Roberto's problem?",
			 "Picture": "",
			 "Item_1": "His leg hurts.",
			 "Item_2": "His head hurts.",
			 "Item_3": "His back hurts.",
			 "Item_4": "His stomach hurts.",
			 "Answer": "His leg hurts.",
			 "Explain": "Roberto怎麼了?/nA.他腿痛。/nB.他頭痛。/nC.他背痛。/nD.他胃痛。/n正確答案為A。/n/nScript:/nDoctor: How are you today, Roberto?/nRoberto: I'm fine, except my leg hurts all the time./nDoctor: I see. Let's check it out. Where does it hurt?/nRoberto: My leg hurts right here near the knee./nDoctor: We should probably take some X-rays./n醫生:Roberto你今天好嗎?/nRoberto:我很好，但是我的腿一直很疼痛。/n醫生:我瞭解了。我們來檢查一下。是哪裡痛呢?/nRoberto:就在膝蓋附近的地方很痛。/n醫生:我們似乎應該照個X光片。"
		  },
		  {
			 "ModuleName": "ListeningMCQ",
			 "Part": "2",
			 "Type": "Listening",
			 "GroupID": "46",
			 "ID": "58",
			 "Hint": "依談話內容，選擇最合適的答案。",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "en_level1a_58.mp3",
			 "Question": "What is Vien's problem?",
			 "Picture": "",
			 "Item_1": "Her ear hurts when she goes outside.",
			 "Item_2": "Her chest hurts when she sleeps.",
			 "Item_3": "Her stomach hurts when she goes outside.",
			 "Item_4": "Her foot hurts when she walks",
			 "Answer": "Her ear hurts when she goes outside.",
			 "Explain": "Vien怎麼了?/nA.她外出的時候耳朵會痛。/nB.她睡覺時胸口會痛。/nC.她外出的時候會胃痛。/nD.她走路時腳會痛。/n正確答案為A。/n/nScript:/nVien: Doctor, I have a terrible earache./nDoctor: Does it hurt all the time?/nVien: Yes, but especially when I am outside./nDoctor: Hmm. You may have some kind of infection./nVien:醫生，我耳朵感到嚴重的疼痛。/n醫生:這痛感一直持續著嗎?/nVien:是的，由其是當我在戶外的時候。/n醫生:嗯，你可能有受到感染。"
		  },
		  {
			 "ModuleName": "ListeningMCQ",
			 "Part": "2",
			 "Type": "Listening",
			 "GroupID": "47",
			 "ID": "59",
			 "Hint": "依談話內容，選擇最合適的答案。",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "en_level1a_59.mp3",
			 "Question": "What is Tino's problem?",
			 "Picture": "",
			 "Item_1": "His back and chest hurt.",
			 "Item_2": "His feet and elbow hurt.",
			 "Item_3": "His legs hurts.",
			 "Item_4": "His teeth hurt.",
			 "Answer": "His feet and elbow hurt.",
			 "Explain": "Tino怎麼了?/nA.他的背和胸部痛。/nB.他的腳和手肘痛。/nC.他的腿痛。/nD.他的牙齒痛。/n正確答案為B。/n/nScript:/nDoctor: Well Tino, it seems like you are here every week these days./nTino: I guess so, Doctor. My feet are killing me!/nDoctor: I know that you were here last week because of your elbow. Didn't the prescription help?/nTino: Not at all. It seems to be getting worse./n醫生:Tino，你最近似乎每個星期都會來這報到。/nTino:醫生，似乎是這樣。我的腳痛死了。/n醫生:我記得你上星期來是因為手肘痛。我開的處方沒有用嗎?/nTino:一點幫助也沒有。而且似乎越來越嚴重。"
		  },
		  {
			 "ModuleName": "ListeningMCQ",
			 "Part": "2",
			 "Type": "Listening",
			 "GroupID": "48",
			 "ID": "60",
			 "Hint": "依談話內容，選擇最合適的答案。",
			 "GroupQuestion": "",
			 "GroupPicture": "",
			 "GroupAudio": "en_level1a_60.mp3",
			 "Question": "What is Eric's problem?",
			 "Picture": "",
			 "Item_1": "His head hurts after eating.",
			 "Item_2": "His head hurts in the morning.",
			 "Item_3": "His stomach hurts after eating.",
			 "Item_4": "His stomach hurts in the morning.",
			 "Answer": "His stomach hurts after eating.",
			 "Explain": "Eric怎麼了?/nA.吃完飯後他會頭痛。/nB.他早上會頭痛。/nC.吃完飯後他會胃痛。/nD.他早上會胃痛。/n正確答案為C。/n/nScript:/nEric: Doctor, I really need your help./nDoctor: What can I do for you?/nEric: My stomach hurts, especially after I eat anything./nDoctor: Well, let's see if we can find something to help you./nEric:醫生，我真的很需要你的協助。/n醫生:你需要什麼協助呢?/nEric:我的胃很痛，由其是當我吃完飯後。/n醫生:嗯，我們來看看一下看看能怎麼幫你。"
		  }
	   ]
	}
}
