var data = {
    "DocumentElement": {
        "Info": {
            "ExamID": "Kr_1a_ver2",
            "ExamTitle": "韓語一級升級測驗",
            "ExamType": "upgrade",
            "ExamClass": "一級",
            "ExamLanguage": "Korean",
            "ExamTime": "40"
        },
        "Exam": [
            {
                "ModuleName": "ListeningMCQ",
                "Part": "1",
                "Type": "듣기",
                "GroupID": "1",
                "ID": "1",
                "Hint": "聽完問題後，請選出正確的回答",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "Kr_1a_46.mp3",
                "Question": "",
                "Picture": "",
                "Item_1": "네, 집을 삽니다.",
                "Item_2": "네, 집에서 사습니다.",
                "Item_3": "아니요, 집을 있습니다.",
                "Item_4": "아니요, 집에서 안 삽니다.",
                "Answer": "아니요, 집에서 안 삽니다.",
                "Explain": "題目翻譯：<br>住家裡嗎?<br>삽니까有可能為살다或사다。<br>ㄹ遇到ㅂ,ㅅ,ㄴ,으會脫落，因此剩下사+ㅂ니까->삽니까。此處的삽니까即是살다+ㅂ니까的結果。回答時，可藉由前方助詞幫助判斷。助詞為受詞助詞을/를，表示前方名詞是被作用的「物品」。而場所助詞에서，是用來表示前方名詞是動作發生的「場所」，由此可知題目中的집是場所，回答時的집語意必須相同才行。<br><br>選項翻譯及解析：<br>(A)對，我買房子。此處房子是受詞，視為「物品」，跟題目「場所」不符。<br>(B)對，我在家裡買。動詞사다沒有尾音，應加ㅂ니다，變成삽니다。<br>(C)不是，我有房子。中文語意上可以說得通，但是있다為形容詞，前方助詞不能使用受詞助詞을/를，應使用이/가。<br>(D)不是，我不住家裡。집語意與題目相同，動詞使用也正確，前方為否定，後面也有跟著否定。因此答案為(Ｄ)。"
            },
            {
                "ModuleName": "ListeningMCQ",
                "Part": "1",
                "Type": "듣기",
                "GroupID": "2",
                "ID": "2",
                "Hint": "聽完問題後，請選出正確的回答",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "Kr_1a_47.mp3",
                "Question": "",
                "Picture": "",
                "Item_1": "네, 한국에 갑니다.",
                "Item_2": "네, 한국에 갔습니다.",
                "Item_3": "아니요, 한국에 있습니다.",
                "Item_4": "아니요, 한국에 갑니다.",
                "Answer": "네, 한국에 갔습니다.",
                "Explain": "題目翻譯：<br>昨天去了韓國嗎?<br>此處使用了過去式，因為時間點是昨天。<br><br>選項翻譯及解析：<br>(A)對，去韓國。昨天發生的事須使用過去式。<br>(B)對，去了韓國。因此答案為(B)。<br>(C)沒有，我在韓國。前後語意矛盾。<br>(D)沒有，去韓國。前後語意矛盾，時態錯誤。"
            },
            {
                "ModuleName": "ListeningMCQ",
                "Part": "1",
                "Type": "듣기",
                "GroupID": "3",
                "ID": "3",
                "Hint": "聽完問題後，請選出正確的回答",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "Kr_1a_48.mp3",
                "Question": "",
                "Picture": "",
                "Item_1": "어머니께서 의사십니다.",
                "Item_2": "어머니가 청소합니다.",
                "Item_3": "어머니께 선생님이십니다.",
                "Item_4": "어머니께 밥을 먹으십니다.",
                "Answer": "어머니께서 의사십니다.",
                "Explain": "題目翻譯：媽媽在做什麼?<br>此處有兩種解釋，一種為現在正在做什麼，另一種則是詢問職業。<br><br>選項翻譯：<br>(A)媽媽是醫生。因此答案為(A)。<br>(B)媽媽在打掃。媽媽為長輩，動詞必須加上敬語(으)시，因此청소하다須改成청소하시다。<br>(C)媽媽是老師。께是授予助詞，意思為給(某人)。께서才是主詞助詞的敬語。<br>(D)媽媽在吃飯。먹다敬語型為드시다，應改為드십니다."
            },
            {
                "ModuleName": "ListeningMCQ",
                "Part": "1",
                "Type": "듣기",
                "GroupID": "4",
                "ID": "4",
                "Hint": "聽完問題後，請選出正確的回答",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "Kr_1a_49.mp3",
                "Question": "",
                "Picture": "",
                "Item_1": "차가 많이 막혔어서 늦었어요.",
                "Item_2": "차가 많이 막혀서 늦었어요.",
                "Item_3": "비기 때문에 늦었어요.",
                "Item_4": "비가 오 때문에 늦었어요.",
                "Answer": "차가 많이 막혀서 늦었어요.",
                "Explain": "題目翻譯：為什麼遲到？<br><br>選項翻譯：<br>(A)因為塞車所以遲到。아서/어서/여서不可加時態。<br>(B)因為塞車所以遲到。因此答案為(B)<br>(C)因為雨所以遲到。기 때문에前面須加動詞或形容詞。名詞依據語意加上이기 때문에或때문에。<br>(D)因為下雨，所以遲到。因為비가 오다動詞所以要加기 때문에才正確。"
            },
            {
                "ModuleName": "ListeningMCQ",
                "Part": "1",
                "Type": "듣기",
                "GroupID": "5",
                "ID": "5",
                "Hint": "聽完對話後，請選出最符合對話內容的答案",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "Kr_1a_52.mp3",
                "Question": "남자는 무엇을 하고 싶은지 고르십시오.",
                "Picture": "",
                "Item_1": "여자와 연습하고 싶습니다.",
                "Item_2": "한국어를 잘하고 싶습니다.",
                "Item_3": "한국 친구를 사귀고 싶습니다.",
                "Item_4": "언어교류회에 가고 싶습니다.",
                "Answer": "한국어를 잘하고 싶습니다.",
                "Explain": "對話翻譯：<br> : 雅婷妳韓文說的真好。我也想像妳一樣說的那麼好。<br> : 那你跟韓國人練習看看啊。<br> : 我也曾經打算那麼做。但是我沒有韓國朋友。<br> : 那下次跟我去語言交流會吧。<br><br>題目翻譯：<br>請選出男生想做什麼。<br><br>選項翻譯：<br>(A)想要和女生練習。<br>(B)想要韓語很厲害。因此答案為(B)<br>(C)想要交韓國朋友。<br>(D)想要去語言交流會。"
            },
            {
                "ModuleName": "ListeningMCQ",
                "Part": "2",
                "Type": "듣기",
                "GroupID": "5",
                "ID": "6",
                "Hint": "聽完對話後，請選出符合對話內容的答案",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "Kr_1a_52.mp3",
                "Question": "들은 내용과 같은 것을 고르십시오.",
                "Picture": "",
                "Item_1": "남자는 한국어를 못합니다.",
                "Item_2": "남자는 한국 친구가 없습니다.",
                "Item_3": "남자는 연습을 싫어합니다.",
                "Item_4": "남자는 언어교류회에 갔습니다.",
                "Answer": "남자는 한국 친구가 없습니다.",
                "Explain": "對話翻譯：<br> : 雅婷妳韓文說的真好。我也想像妳一樣說的那麼好。<br> : 那你跟韓國人練習看看啊。<br> : 我也曾經打算那麼做。但是我沒有韓國朋友。<br> : 那下次跟我去語言交流會吧。<br><br>題目翻譯：<br>請選出與聽到的內容相符的答案。<br><br>選項翻譯：<br>(A)男生不會說韓語。<br>(B)男生沒有韓國朋友。<br>(C)男生不喜歡練習。<br>(D)男生去過語言交流會。<br><br>解析：<br>(A)男生想要韓語說得厲害，表示他有一定程度才想進步。因此並非不會說韓語。<br>(B)對話裡男生很明確地說出自己沒有韓國朋友。因此答案為(B)<br>(C)對話裡沒有提到對於練習的態度。<br>(D)對話裡沒有提到是否參加過語言交流會。<br>"
            },
            {
                "ModuleName": "ListeningMCQ",
                "Part": "2",
                "Type": "듣기",
                "GroupID": "7",
                "ID": "7",
                "Hint": "聽完對話後，請選出符合對話內容的答案",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "Kr_1a_53.mp3",
                "Question": "남자가 왜 이 이야기를 하고 있는지 고르십시오.",
                "Picture": "",
                "Item_1": "휴가를 신청하려고 합니다.",
                "Item_2": "여자와 같이 가려고 합니다.",
                "Item_3": "한국에 가려고 합니다.",
                "Item_4": "재미있는 관광지를 물어보려고 합니다.",
                "Answer": "재미있는 관광지를 물어보려고 합니다.",
                "Explain": "對話翻譯：<br>：我從10月11日到10月14日要去韓國。<br>：真假? 好好喔!<br>：妳去過韓國對吧? 哪裡比較好啊?<br>：對阿，我喜歡釜山。你也去一次看看吧。<br><br>題目翻譯：<br>請選出男生提起這個話題的理由。<br><br>選項翻譯：<br>(A)打算請假。<br>(B)打算和女生一起去。<br>(C)打算去韓國。<br>(D)打算問觀光景點哪裡好玩。<br><br>解析：<br>(A)對話中沒有提到請假。<br>(B)對話中沒有提到是否邀請女生一起去。<br>(C)雖然是打算去韓國，但並非主要提起此話題的原因。<br>(D)由於女生去過韓國，因此想問是否有推薦的景點。因此答案為(D)"
            },
            {
                "ModuleName": "ListeningMCQ",
                "Part": "2",
                "Type": "듣기",
                "GroupID": "7",
                "ID": "8",
                "Hint": "聽完對話後，請選出符合對話內容的答案",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "Kr_1a_53.mp3",
                "Question": "들은 내용과 같은 것을 고르십시오.",
                "Picture": "",
                "Item_1": "남자는 한국에 4일 동안 있을 겁니다.",
                "Item_2": "여자는 한국에 안 가 봤습니다.",
                "Item_3": "남자는 부산에 가려고 했습니다.",
                "Item_4": "여자는 서울을 좋아합니다.",
                "Answer": "남자는 한국에 4일 동안 있을 겁니다.",
                "Explain": "對話翻譯：<br>：我從10月11日到10月14日要去韓國。<br>：真假? 好好喔!<br>：妳去過韓國對吧? 哪裡比較好啊?<br>：對阿，我喜歡釜山。你也去一次看看吧。<br><br>題目翻譯：<br>請選出與聽到的內容相符的答案。<br><br>選項翻譯：<br>(A)男生會去韓國4天。<br>(B)女生沒去過韓國。<br>(C)男生原本打算要去釜山。<br>(D)女生喜歡首爾。<br><br>解析：<br>(A)從10/11到10/14共4天。因此答案為(A)<br>(B)男生向女生確認是否去過韓國，女生回答是,有去過。<br>(C)對話中最後沒有提到男生的選擇。<br>(D)女生喜歡釜山。<br>"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "단어",
                "GroupID": "9",
                "ID": "9",
                "Hint": "請選擇文章在談論的主題",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "자장면은 오천 원입니다. 짬뽕은 칠천 원입니다.",
                "Picture": "",
                "Item_1": "옷",
                "Item_2": "값",
                "Item_3": "일",
                "Item_4": "맛",
                "Answer": "값",
                "Explain": "題目翻譯：<br>炸醬麵5000韓元。辣海鮮麵7000元。<br>選項翻譯：<br>(A)衣服<br>(B)價格，故選(B)<br>(C)事情<br>(D)味道"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "단어",
                "GroupID": "10",
                "ID": "10",
                "Hint": "請完成克漏字填空",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "지우개 세 ____하고 볼펜 두 ____ 샀습니다.",
                "Picture": "",
                "Item_1": "개;권",
                "Item_2": "개;자루",
                "Item_3": "병;자루",
                "Item_4": "그릇;잔",
                "Answer": "개;자루",
                "Explain": "題目翻譯: 買了三個橡皮擦和兩支原子筆<br><br>選項解析: 須選擇正確量詞<br>(A)개：個； 권：本，不符題意。<br>(B)개：個；자루 ：支，符合題意故選(B)。<br>(C)병：瓶； 자루 ：支，不符題意。<br>(D) 그릇：碗； 잔：杯，不符題意。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "단어",
                "GroupID": "11",
                "ID": "11",
                "Hint": "請完成克漏字填空",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "지금 오전 세 시 삼십 __입니다.",
                "Picture": "",
                "Item_1": "월",
                "Item_2": "일",
                "Item_3": "시간",
                "Item_4": "분",
                "Answer": "분",
                "Explain": "題目翻譯: 現在是上午三點三十分<br><br>選項解析:<br>(A)月，不符題意。<br>(B)日，不符題意。<br>(C)小時，不符題意。<br>(D)分，故選(D)。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "단어",
                "GroupID": "12",
                "ID": "12",
                "Hint": "請完成克漏字填空",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "저는 철수입니다. 한국___삽니다.",
                "Picture": "",
                "Item_1": "하고",
                "Item_2": "의",
                "Item_3": "에서",
                "Item_4": "이",
                "Answer": "에서",
                "Explain": "題目翻譯：<br>我是哲秀。在韓國生活。<br><br>選項解析：<br>(A)하고是「和」的意思，語意不符。<br>(B)의是「的」的意思，語意不符。<br>(C)에서是「在」的意思，符合題意，故選(C)。<br>(D)이是主格助詞，但並不是韓國在生活，而是人在生活，因此不符題意。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "단어",
                "GroupID": "13",
                "ID": "13",
                "Hint": "請完成克漏字填空",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "수업이 있습니다. 학교___  갑니다.",
                "Picture": "",
                "Item_1": "에서",
                "Item_2": "와 ",
                "Item_3": "에",
                "Item_4": "의",
                "Answer": "에",
                "Explain": "題目翻譯:有課，要去學校。<br><br>選項解析: 本題應該要選場所助詞<br>(A) 強調狀態/行為 發生場所的助詞，與題意不符。<br>(B) 「跟/和」的意思，語意不符。<br>(C)  場所助詞，符合題意，故選(C)<br>(D) 「的」的意思，與題意不符。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "단어",
                "GroupID": "14",
                "ID": "14",
                "Hint": "請完成克漏字填空",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "언니는 과자를 좋아하지 않습니다. 그래서 과자를 ___먹습니다.",
                "Picture": "",
                "Item_1": "안",
                "Item_2": "아까",
                "Item_3": "자주",
                "Item_4": "제일",
                "Answer": "안",
                "Explain": "題目翻譯: 姐姐不喜歡零食。所以不吃零食。<br><br>選項解析:<br>(A) 否定副詞，即為「不」的意思，符合題意，故選(A)。<br>(B) 「剛剛」，語意不符。<br>(C)「常常」，語意不符。<br>(D) 「最」，語意不符。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "단어",
                "GroupID": "15",
                "ID": "15",
                "Hint": "請完成克漏字填空",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "여행을 다닙니다. 가족들과 사진을 ___.",
                "Picture": "",
                "Item_1": "찍습니다.",
                "Item_2": "배웁니다.",
                "Item_3": "마십니다.",
                "Item_4": "만납니다.",
                "Answer": "찍습니다.",
                "Explain": "題目翻譯:去旅行。和家人拍照片。<br><br>選項解析:<br>(A)사진을 찍다為拍照的慣用法，故選(A)<br>(B)「學習」的意思，不符題意。<br>(C)「喝」的意思，不符題意。<br>(D)「見」的意思，不符題意。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "단어",
                "GroupID": "16",
                "ID": "16",
                "Hint": "請完成克漏字填空",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "A: 할아버지께서 _____가 어떻게 되세요?<br>B: 올해로 쉰여덟 ____ 입니다.",
                "Picture": "",
                "Item_1": "성함;세",
                "Item_2": "연세;세",
                "Item_3": "진지;살",
                "Item_4": "연세;살",
                "Answer": "연세;살",
                "Explain": "題目翻譯: A: 爺爺今年貴庚? B:今年58歲<br><br>解析:<br>(A)「姓名」的敬語；歲(+漢語數詞)<br>(B)「年紀」的敬語；歲(+漢語數詞)<br>(C)「飯」的敬語；歲(+韓語數詞)<br>(D)「年紀」的敬語；歲(+韓語數詞)，故選(D)。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "문법",
                "GroupID": "17",
                "ID": "17",
                "Hint": "請選擇最合適的答案",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "지하철___ 회사에 갑니다.",
                "Picture": "",
                "Item_1": "으로",
                "Item_2": "를",
                "Item_3": "을",
                "Item_4": "로",
                "Answer": "로",
                "Explain": "題目翻譯: 搭地鐵去公司。<br><br>選項解析: 須選擇正確的助詞<br>(A)表示手段，工具之助詞。有尾音名詞(除ㄹ外) + 으로，不符題意。<br>(B)受格助詞。無尾音名詞 + 를，不符題意。<br>(C) 受格助詞。有尾音名詞 + 을，不符題意。<br>(D) 表示手段，工具之助詞。ㄹ尾音以及無尾音名詞+ 로，故選(D)。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "문법",
                "GroupID": "18",
                "ID": "18",
                "Hint": "請選擇最合適的答案",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "내일 친구___ 같이 노래방에서 노래합니다.",
                "Picture": "",
                "Item_1": "과",
                "Item_2": "와",
                "Item_3": "의",
                "Item_4": "가",
                "Answer": "와",
                "Explain": "題目翻譯:明天和朋友一起在KTV唱歌。<br><br>選項解析:<br>(A)須接有尾音的名詞，不符題意。<br>(B)須接無尾音的名詞，故選(B)<br>(C)「的」之意，不符題意。<br>(D)主格助詞，因後方有같이故可推知此處應選表示「和」的助詞，與題意不符。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "문법",
                "GroupID": "19",
                "ID": "19",
                "Hint": "請選擇最合適的答案",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "저는 어제 백화점에서 바지를 샀습니다. 신발___  샀습니다.",
                "Picture": "",
                "Item_1": "만",
                "Item_2": "도",
                "Item_3": "을",
                "Item_4": "를",
                "Answer": "도",
                "Explain": "題目翻譯:我昨天在百貨公司買褲子。也買了鞋子。<br><br>選項解析:<br>(A)「只」的意思，與題意不符。<br>(B)「也」，故選(B)。<br>(C)受格助詞。有尾音名詞 + 을，與題意不符。<br>(D)受格助詞。無尾音名詞 + 를，與題意不符。<br>"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "문법",
                "GroupID": "20",
                "ID": "20",
                "Hint": "請選擇最合適的答案",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "모레 친구의 결혼식입니다. 저는 친구_____ 주려고 선물을 준비했습니다.",
                "Picture": "",
                "Item_1": "께서",
                "Item_2": "를",
                "Item_3": "에게",
                "Item_4": "에",
                "Answer": "에게",
                "Explain": "題目翻譯:後天是朋友的婚禮。我把要給朋友的禮物準備好了<br><br>選項解析:<br>(A) 主格助詞 이/가 的敬語，不符題意。<br>(B) 受格助詞。無尾音名詞 + 를，不符題意。<br>(C) 書面語，表示動作的對象，符合題意故選(C)。<br>(D)場所助詞，與本題無關。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "문법",
                "GroupID": "21",
                "ID": "21",
                "Hint": "請選擇最合適的答案",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "기차로 타이베이___ 가오슝까지 내려갔습니다.",
                "Picture": "",
                "Item_1": "에서",
                "Item_2": "에",
                "Item_3": "는",
                "Item_4": "은",
                "Answer": "에서",
                "Explain": "題目翻譯: 搭火車從台北下去高雄。<br><br>選項解析: <br>(A)~에서~까지表示從(起點)到(終點)，符合題意，故選(A)<br>(B)場所助詞，不符題意。<br>(C)主題/對比用助詞。無尾音名詞 + 는，不符題意。<br>(D)主題/對比用助詞。有尾音名詞 + 은，不符題意。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "문법",
                "GroupID": "22",
                "ID": "22",
                "Hint": "請選擇最合適的答案",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "동생은 여행을 좋아합니다. 산___바다를 더 좋아합니다. 그래서 바다에 자주 갑니다.",
                "Picture": "",
                "Item_1": "이",
                "Item_2": "은",
                "Item_3": "에서",
                "Item_4": "보다",
                "Answer": "보다",
                "Explain": "題目翻譯:弟弟喜歡旅行。比起山更喜歡海。所以常常去海邊。<br><br>選項解析:<br>(A)主格助詞，此句應為比較關係，不符題意。<br>(B)表示主題、對比之助詞，末句只說常去海邊，可推知前句為比較關係，不符題意。<br>(C)強調狀態/行為 發生場所的助詞，與題意不符。<br>(D)「比起…」的意思，符合題意，故選(D)"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "문법",
                "GroupID": "23",
                "ID": "23",
                "Hint": "請選擇最合適的答案",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "다리가 아프면 천천히 ________",
                "Picture": "",
                "Item_1": "걸으세요",
                "Item_2": "걷으세요.",
                "Item_3": "걸려요.",
                "Item_4": "걷아요.",
                "Answer": "걸으세요",
                "Explain": "題目翻譯:如果腳不舒服，請慢慢走。<br><br>選項解析:<br>(A)ㄷ不規則變化，遇母音為首的字時須變為ㄹ。걸으세요符合題意，故選(A)<br>(B)需變化為걸으세요，不符題意。<br>(C)「花費」的意思，不符題意。<br>(D)無此用法，不符題意。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "문법",
                "GroupID": "24",
                "ID": "24",
                "Hint": "請選擇最合適的答案",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "숙제를 ________ 반 친구에게 물었습니다.",
                "Picture": "",
                "Item_1": "몰라서",
                "Item_2": "모르어서",
                "Item_3": "몰라지만",
                "Item_4": "모르지만",
                "Answer": "몰라서",
                "Explain": "題目翻譯:因為不會作業，所以問班上同學。<br><br>選項解析:<br>(A)르不規則變化，遇아/어時，르的ㄹ脫落，모加上ㄹ尾音，且모為陽性故後面加아。아/어서是「因為...」的文法，符合題意，故選(A)。<br>(B)未依르不規則變化，不符題意。<br>(C)無此用法。<br>(D)「雖然...但是...」的文法，不符題意。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "문법",
                "GroupID": "25",
                "ID": "25",
                "Hint": "請選擇最合適的答案",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "저는 후배가 마음에 듭니다. 오늘 후배한테 __________.",
                "Picture": "",
                "Item_1": "고백하고 싶습니다.",
                "Item_2": "고백하고 싶어합니다.",
                "Item_3": "헤어지고 싶어합니다.",
                "Item_4": "헤어지고 싶습니다.",
                "Answer": "고백하고 싶습니다.",
                "Explain": "題目翻譯:我喜歡一位後輩。今天想和後輩告白。<br><br>選項解析:<br>(A)고 싶다是「想做~」的意思，符合題意，故選(A)。<br>(B)고 싶어하다是「想做~」的意思，但主詞需為第三人稱，故不符題意。<br>(C)헤어지다是「分開」的意思加上고 싶어하다是「想做~」的意思，但主詞需為第三人稱，故不符題意。<br>(D)헤어지다是「分開」的意思，不符題意。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "문법",
                "GroupID": "26",
                "ID": "26",
                "Hint": "請選擇最合適的答案",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "마트에서 고기를 샀습니다. 오늘 친구 집에서 불고기를 _________.",
                "Picture": "",
                "Item_1": "만들시다.",
                "Item_2": "만듭시다.",
                "Item_3": "닦을시다.",
                "Item_4": "닦읍시다.",
                "Answer": "만듭시다.",
                "Explain": "題目翻譯:在超市買肉。今天要在朋友家烤牛肉。<br>選項解析:<br>(A)ㄹ自動脫落變만듭시다，不符題意。<br>(B)\"ㄹ\"遇到 ㅂ開頭的 ㅂ시다 ，ㄹ自動脫落變만듭시다，符合題意故選(B)。<br>(C)無此用法。<br>(D)「擦」的意思， 不符題意。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "문법",
                "GroupID": "27",
                "ID": "27",
                "Hint": "請選擇最合適的答案",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "날씨가 ________ 창문을 열어 주세요.",
                "Picture": "",
                "Item_1": "덥어서",
                "Item_2": "더워서",
                "Item_3": "덥우니까",
                "Item_4": "더우니까",
                "Answer": "더우니까",
                "Explain": "題目翻譯: 因為天氣熱，所以請開窗戶。<br><br>解析:<br>(A)無此用法。<br>(B)아/어서表示原因的意思，但後面不可接共動句及命令句，不符題意。<br>(C)無此用法。<br>(D)符合ㅂ不規則變化。(으)니까表示原因的意思，且後面可加共動句及命令句，故選(D)。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "문법",
                "GroupID": "28",
                "ID": "28",
                "Hint": "請選擇最合適的答案",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "돈이 없으면 타이베이에서 집을 _______. ",
                "Picture": "",
                "Item_1": "살을 수 없어요.",
                "Item_2": "살 수 있어요.",
                "Item_3": "살 수 없어요.",
                "Item_4": "살을 수 있어요.",
                "Answer": "살 수 없어요.",
                "Explain": "題目翻譯: 沒錢的話在台北不能買房子。<br><br>解析:<br>(A)無此用法<br>(B)可以買<br>(C)不能買，故選(C)。<br>(D)無此用法"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "읽기",
                "GroupID": "29",
                "ID": "29",
                "Hint": "請閱讀下方內容後，選擇最合適的答案",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "오늘 백화점에서 옷을 하나 샀습니다. 옷의 디자인이 정말 예쁩니다. 값도 비싸지 않아서 좋습니다.",
                "Picture": "",
                "Item_1": "오늘 산 옷은 비쌉니다.",
                "Item_2": "옷의 디자인이 안 예쁩니다.",
                "Item_3": "옷의 디자인이 예쁘지만 값이 비쌉니다.",
                "Item_4": "저는 예쁜 옷을 샀습니다.",
                "Answer": "저는 예쁜 옷을 샀습니다.",
                "Explain": "題目翻譯: 今天在百貨公司買了一件衣服。衣服的設計非常漂亮。價錢也不貴，所以喜歡。<br><br>解析:<br>(A)今天買的衣服很貴。<br>(B)衣服的設計不漂亮。<br>(C)衣服的設計漂亮但價錢很貴。<br>(D)我買了漂亮的衣服，故選(D)。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "읽기",
                "GroupID": "30",
                "ID": "30",
                "Hint": "請閱讀下方內容後，選擇最合適的答案",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "저는 주말에 고향에 내려갑니다. 가족들과 친구를 만날 수 있어서 기분이 좋습니다. 고향에 돌아가기 전에 어머니께 전화 드려야겠습니다.",
                "Picture": "",
                "Item_1": "저는 주말에 친구 집에 가려고 합니다.",
                "Item_2": "저는 어머니께 전화 드렸습니다.",
                "Item_3": "저는 기분이 좋습니다.",
                "Item_4": "저는 친구를 못 만납니다.",
                "Answer": "저는 기분이 좋습니다.",
                "Explain": "題目翻譯:我周末要回故鄉，可以見到家人和朋友所以心情很好。回故鄉前要先打電話給媽媽。<br><br>解析:<br>(A)我周末打算去朋友家。<br>(B)我已經打電話給媽媽了。<br>(C)我心情很好，故選(C)。<br>(D)我不能見朋友。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "읽기",
                "GroupID": "31",
                "ID": "31",
                "Hint": "請閱讀下方內容後，選擇最合適的答案",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "시간이 있을 때마다 친구하고 운동장에서 테니스를 칩니다. 친구가 이번 주말에 바빠서 같이 운동을 못 합니다. 그래서 주말에 혼자 농구장에서 농구하려고 합니다. ",
                "Picture": "",
                "Item_1": "이번 주말에 친구와 같이 테니스를 칩니다.",
                "Item_2": "시간이 있을 때마다 농구를 합니다.",
                "Item_3": "이번 주말에 친구가 안 바쁩니다.",
                "Item_4": "이번 주말에 혼자 농구를 하려고 합니다.",
                "Answer": "이번 주말에 혼자 농구를 하려고 합니다.",
                "Explain": "題目翻譯:每當有時間的時候會跟朋友去運動場打網球。朋友這周末很忙所以不能一起運動。因此周末打算自己去籃球場打籃球。<br><br>解析:<br>(A)這個周末和朋友一起打網球。<br>(B)每當有時間的時候就會打籃球。<br>(C)這周末朋友不忙。<br>(D)這個周末打算自己去打籃球，故選(D)。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "읽기",
                "GroupID": "32",
                "ID": "32",
                "Hint": "請閱讀下方內容後，選擇最合適的答案",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "친구 머리가 아파서 학교에 못 왔습니다. 내일 오후에 친구를 만나서 숙제를 말해 줄 겁니다. 그리고 친구를 데리고 병원에 갈 겁니다.",
                "Picture": "",
                "Item_1": "저는 친구를 도와주고 싶습니다.",
                "Item_2": "저는 친구와 함께 학교에서 숙제를 할 겁니다.",
                "Item_3": "친구와 같이 학교에 갈 겁니다.",
                "Item_4": "저는 머리가 아파서 병원에 갈 겁니다.",
                "Answer": "저는 친구를 도와주고 싶습니다.",
                "Explain": "題目翻譯: 朋友頭痛所以不能來學校。明天下午見到朋友要告訴她作業。還有要帶朋友去看醫生。<br><br>解析:<br>(A)我想幫朋友忙，故選(A)。<br>(B)我會和朋友在學校寫作業。<br>(C)和朋友一起去學校。<br>(D)我因為頭痛所以要去醫院。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "읽기",
                "GroupID": "33",
                "ID": "33",
                "Hint": "請閱讀下方內容後，選擇最合適的答案",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "저는 혼자 산책하는 것을 좋아합니다. 보통  ____  회사 근처에서 산책합니다. 큰 길보다 작은 길을 다닙니다. ",
                "Picture": "",
                "Item_1": "퇴근하지만",
                "Item_2": "퇴근한 후에",
                "Item_3": "퇴근하니까",
                "Item_4": "퇴근하려면",
                "Answer": "퇴근한 후에",
                "Explain": "題目翻譯:我喜歡一個人散步。平常下班後在公司附近散步，比起大馬路我都走小路。<br><br>解析:<br>(A)지만是「但是」的意思。<br>(B)(으)ㄴ 후에為「~之後」的意思，故選(B)。<br>(C)니까為「因為...所以...」的意思。<br>(D)려면為「打算...的話」的意思。"
            },
            {
                "ModuleName": "ReadingMCQ",
                "Part": "1",
                "Type": "읽기",
                "GroupID": "33",
                "ID": "34",
                "Hint": "請閱讀下方內容後，選擇最合適的答案",
                "GroupQuestion": "",
                "GroupPicture": "",
                "GroupAudio": "",
                "Question": "저는 혼자 산책하는 것을 좋아합니다. 보통 퇴근한 후에 회사에서 집까지 걸어갑니다.  그리고 밤에 밥을 먹은 후에도 공원에 가서 산책합니다.",
                "Picture": "",
                "Item_1": "저는 보통 집에서 산책합니다.",
                "Item_2": "저는 여러 사람과 같이 산책합니다.",
                "Item_3": "저는 보통 출근하기 전에 산책합니다.",
                "Item_4": "저는 저녁 식사 후 공원에 가서 산책합니다.",
                "Answer": "저는 저녁 식사 후 공원에 가서 산책합니다.",
                "Explain": "題目翻譯:我喜歡一個人散步。平常下班後都走路回家，晚上吃完飯後也會去公園散步。<br><br>解析:<br>(A)我平常在家散步。<br>(B)我和許多人一起散步。<br>(C)我通常在上班前散步。<br>(D)我吃完晚餐去公園散步，故選(D)。"
            }
        ]
    }
}