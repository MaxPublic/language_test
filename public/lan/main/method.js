var module;
var ModuleSet = {};
function getValue(str){
	var value="";
	if(str){
		value = trim(str).replace(/\/n/g,"<br>");
	}
	return value;
}
function getClearValue(str){
	var value="";
	if(str){
		value = trim(str).replace(/\/n/g,"");
	}
	return value;
}
function getLocalPath(){ //=====取得路徑=====
	var url=location.href;
	var Path=url.substr(0,url.lastIndexOf("/")+1);
	var arrTemp, url1;
	if(Path.lastIndexOf("/")!=-1) {
		Path=Path.substr(0, Path.lastIndexOf("/")+1);	
		if(Path.indexOf("///")!=-1) {
			arrTemp=Path.split("///");
			url1=arrTemp[1];		
		} else {
			url1=Path;
		}
		if(url1.indexOf("file:")!=-1) {
			arrTemp=url1.split("file:");
			url1=arrTemp[1];	
		}
		url1=url1;
	}
	return url1;
}
function getPointPath(){
	var Path="../";
	return Path;
}
function gethtmFile(){ //網頁路徑
	var getPath = getLocalPath().split("/");
	var path="";
	for(var i=0;i<getPath.length-2;i=i+1){
		path=path+getPath[i]+"/";
	}
	path=path+"htm/";
	return path;
}
function getImagesFile(){ //圖片路徑
	var getPath = getLocalPath().split("/");
	var path="";
	for(var i=0;i<getPath.length-2;i=i+1){
		path=path+getPath[i]+"/";
	}
	path=path+"images/";
	return path;
}
function getMaterialFile(){ //素材路徑
	var getPath = getLocalPath().split("/");
	var path="";
	for(var i=0;i<getPath.length-1;i=i+1){
		path=path+getPath[i]+"/";
	}
	path=path+"material/";
	return path;
}
function getPage(obj,id){ //取得頁數
	var p=0;
	var tp=0;
	if(obj.DocumentElement.Module[0].ModuleName=="Intro"){
		p = findIndexByKeyValue(obj.DocumentElement.Module, "ID", id);
		tp = obj.DocumentElement.Module.length - 1;
	}else{
		p = findIndexByKeyValue(obj.DocumentElement.Module, "ID", id)+1;
		tp = obj.DocumentElement.Module.length;
	}
	return p+"/"+tp;
}
function usefloor(min,max) { 
	return Math.floor(Math.random()*(max-min+1)+min); 
}
function getRangRandom(num,getNum) //取範圍不重覆亂數(最大值，取多少值) return 數值陣列
{
	var rnd;
	result = new Array();
	rndSet = new Array();
	
	for(var i=0;i<getNum;i++){
		//alert(rndSet[rnd]);
		rnd = usefloor(0,num);
		//alert(rndSet[rnd]+" "+rnd);
		while(rndSet[rnd] !== undefined && rndSet[rnd] === rnd)
			rnd = usefloor(0,num);
		rndSet[rnd]=rnd;
		result[i] = rnd;
	}
	return result;
}
function getNoRepeat(A,B,getNum){ //同型態取A陣列裡多筆資料但與B陣列不重覆(A陣列,B陣列,取多少筆) return 不重覆的陣列
	var newdata = new Array();
	for(var i=0;i<A.length;i=i+1){
		var bool=false;
		for(var j=0;j<B.length;j=j+1){
			//alert(A[i]+"  "+B[j]);
			if(A[i] === B[j]){
				bool=true;
				break;
			}
		}
		if(bool){
			bool=false;
		}else{
			newdata.push(A[i]);
		}
	}
	var rand = getRangRandom(newdata.length-1,getNum);
	var array = new Array();
	for(var i=0;i<getNum;i=i+1){
		array.push(newdata[rand[i]]);
	}
	return array;
}
function secToMin(sec){
	var m,s,timeString,k;
	m = parseInt(sec/60);
	if (String(m).length === 1){
		timeString = "0"+m;
	} else {
		timeString = m;
	}
	k=parseInt(sec-(m*60));
	if (String(sec/60).indexOf(".") !== -1){

		if (String(k).length === 1){
			timeString = timeString+":0"+k;
		} else {
			timeString = timeString+":"+k;
		}
	} else {
		timeString = timeString+":"+"00";
	}
	return timeString;
}

function bubbleStop(e){ //阻止冒泡事件
   if(e.stopPropagation) return e.stopPropagation();
   else return e.cancelBubble=true;            
}
/*
function $(id){
	return document.getElementById(id);
}
*/
function getForObj(obj){ 
	//JSON格式如果預測此Object也有可能為ObjectArray的話，因Object的length為undefined，在for為造成Crash，所以將Object轉為Array的索引0位置
	var temp=new Array();
	if(!obj.length){
		temp[0]=obj;
		obj = temp;
	}
	return obj;
}
ModuleSet.REPLACEBR = '<P style="margin:0; padding:0;">'; //原本為<br>但因需使用縮排及一些因素，所以以<P style="margin:0; padding:0;">來取代
/*
function getValue(obj,name){
	//取值時省略#cdata-section
	var value="";
	try{
		value = obj["#cdata-section"];
		if(value !== "0"){
			value = value.replace(/０/g,"0"); //因xml表單的空值以0代替，所以答案為0時會出錯，因此以全型０代替0在取代回來
			value = value.replace(/’/g,"'"); //取代’，因為在office軟體下打不出來'，會使程式判斷錯誤
			value = trim(value).replace(/\/n/g,ModuleSet.REPLACEBR);
			var pattern = new RegExp(ModuleSet.REPLACEBR+ModuleSet.REPLACEBR,"g"); //兩個P中間不能有空值否則無法斷行，因此用&nbsp;來取代空值
			var matches = value.match(pattern);
			if(matches !== null){
				for(var i=0;i<matches.length;i=i+1){
					value = value.replace(new RegExp(matches[i],'g'),ModuleSet.REPLACEBR+"&nbsp;"+ModuleSet.REPLACEBR);
				}
			}
		}else{
			value = "";
		}
	}catch(e){}
	return value;
}
*/
function getObjects(obj, key, val) {
	var objects = [];
	for (var i in obj) {
	if (!obj.hasOwnProperty(i)) continue;
	if (typeof obj[i] == 'object') {
		objects = objects.concat(getObjects(obj[i], key, val));
	} else
		//if key matches and value matches or if key matches and value is not passed (eliminating the case where key matches but passed value does not)
		if (i == key && obj[i] == val || i == key && val == '') { //
		objects.push(obj);
		} else if (obj[i] == val && key == ''){
			//only add if the object is not already in the array
			if (objects.lastIndexOf(obj) == -1){
			objects.push(obj);
			}
		}
	}
	return objects;
}
 
//返回搜尋值的物件
function getValues(obj, key) {
	var objects = [];
	for (var i in obj) {
		if (!obj.hasOwnProperty(i)) continue;
		if (typeof obj[i] == 'object') {
			objects = objects.concat(getValues(obj[i], key));
		} else if (i == key) {
			objects.push(obj[i]);
		}
	}
	return objects;
}
//返回搜尋值
function getKeys(obj, val) {
	var objects = [];
	for (var i in obj) {
		if (!obj.hasOwnProperty(i)) continue;
		if (typeof obj[i] == 'object') {
			objects = objects.concat(getKeys(obj[i], val));
		} else if (obj[i] == val) {
			objects.push(i);
		}
	}
	return objects;
}
//搜尋排序
function findIndexByKeyValue(obj, key, value) {
    for (var i = 0; i < obj.length; i++) {
        if (obj[i][key] == value) {
            return i;
        }
    }
    return null;
}
function del_u_tag(str){ //刪除底線u
	pattern = /<u>(.*?)<\/u>/g;
	matches = str.match(pattern);
	if(matches !== null){
		for(var i=0;i<matches.length;i=i+1){
			var word=matches[i].replace("<u>","").replace("</u>","");
			str = str.replace(new RegExp(matches[i],'g'),word);
		}
		return str;
	}else{
		return str;
	}
}
function replaceAll(strOrg, strFind, strReplace) {
    var index = 0;
    while (strOrg.indexOf(strFind, index) != -1) {
        strOrg = strOrg.replace(strFind, strReplace);
        index = strOrg.indexOf(strFind, index);
    }
    return strOrg;
}
ModuleSet.REPLACECUTSYMBO = "＠"; //用統一變數ModuleSet.REPLACECUTSYMBO取代所有的切割符號";"
function processStr(str,separated) {
	str = replaceAll(str,separated+separated,separated);
	str = replaceAll(str,separated+separated,separated);
	if(str.substring(0,separated.length) === separated){
		str = str.substring(separated.length,str.length);
	}
	if(str.substring(str.length-separated.length,str.length) === separated){
		str = str.substring(0,str.length-separated.length);
	}
	if(str.charAt(str.length-1) === separated){
		str = str.substring(0,str.length-1);
	}
	return str;
}
function changRadioImg(dom,but){
	var input = but.parentNode.getElementsByTagName("input");
	for(var i=0;i<input.length;i=i+1){
		input[i].parentNode.getElementsByTagName("img")[0].src="images/icon_checkoff.png";
	}
	var imgInput = but.getElementsByTagName("img");
	for(var i=0;i<imgInput.length;i=i+1){
		if(imgInput[i].getAttribute("type")){
			if(imgInput[i].getAttribute("type") === "radio"){
				imgInput[i].src="images/icon_checkon.png";
			}
		}
	}
}
function defaultRadioImg(dom){
	var input = dom.getElementsByTagName("input");
	for(var i=0;i<input.length;i=i+1){
		input[i].parentNode.getElementsByTagName("img")[0].src="images/icon_checkoff.png";
	}
}
function clone(obj) { //適用於大部分對象的深度複製(Deep Copy)。http://noyesno.net/page/javascript/20111212-331
    // Handle the 3 simple types, and null or undefined
    if (null == obj || "object" != typeof obj) return obj;

    // Handle Date
    if (obj instanceof Date) {
        var copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    // Handle Array
    if (obj instanceof Array) {
        var copy = [];
		var len = obj.length
        for (var i = 0; i < len; ++i) {
            copy[i] = clone(obj[i]);
        }
        return copy;
    }

    // Handle Object
    if (obj instanceof Object) {
        var copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
        }
        return copy;
    }

    throw new Error("Unable to copy obj! Its type isn't supported.");
}
function _importParentClass(className){ //import類別至模組裡
	if(!browserTypeof().chrome){
		window[className] = parent[className];
		//eval("window."+className+" = parent."+className);
	}
}

function unBig5(istr){	//全形轉半形
	var strtmp;
	strtmp = istr.replace("’","'")
	//strtmp = istr.replace("(","（")
	//strtmp = replace(strtmp,")","）")
	//strtmp = replace(strtmp,"[","〔")
	//strtmp = replace(strtmp,"]","〕")
	//strtmp = replace(strtmp,"{","｛")
	//strtmp = replace(strtmp,"}","｝")
	//strtmp = replace(strtmp,".","。")
	//strtmp = replace(strtmp,",","，")
	//strtmp = replace(strtmp,";","；")
	//strtmp = replace(strtmp,":","：")
	//strtmp = replace(strtmp,"-","—")
	//strtmp = replace(strtmp,"!","？")
	//strtmp = replace(strtmp,"!","！")
	//strtmp = replace(strtmp,"#","＃")
	//strtmp = replace(strtmp,"$","＄")
	//strtmp = replace(strtmp,"%","％")
	//strtmp = replace(strtmp,"&","＆")
	//strtmp = replace(strtmp,"|","｜")
	//strtmp = replace(strtmp,"","＼")
	//strtmp = replace(strtmp,"/","／")
	//strtmp = replace(strtmp,"+","＋")
	//strtmp = replace(strtmp,"=","＝")
	//strtmp = replace(strtmp,"*","＊")
	//strtmp = replace(strtmp,"0","０")
	//strtmp = replace(strtmp,"1","１")
	//strtmp = replace(strtmp,"2","２")
	//strtmp = replace(strtmp,"3","３")
	//strtmp = replace(strtmp,"4","４")
	//strtmp = replace(strtmp,"5","５")
	//strtmp = replace(strtmp,"6","６")
	//strtmp = replace(strtmp,"7","７")
	//strtmp = replace(strtmp,"8","８")
	//strtmp = replace(strtmp,"9","９")
	return strtmp;
}
function unAsc(text) {  //半形轉全形
  var asciiTable = "!\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";
  var big5Table = "%uFF01%u201D%uFF03%uFF04%uFF05%uFF06%u2019%uFF08%uFF09%uFF0A%uFF0B%uFF0C%uFF0D%uFF0E%uFF0F%uFF10%uFF11%uFF12%uFF13%uFF14%uFF15%uFF16%uFF17%uFF18%uFF19%uFF1A%uFF1B%uFF1C%uFF1D%uFF1E%uFF1F%uFF20%uFF21%uFF22%uFF23%uFF24%uFF25%uFF26%uFF27%uFF28%uFF29%uFF2A%uFF2B%uFF2C%uFF2D%uFF2E%uFF2F%uFF30%uFF31%uFF32%uFF33%uFF34%uFF35%uFF36%uFF37%uFF38%uFF39%uFF3A%uFF3B%uFF3C%uFF3D%uFF3E%uFF3F%u2018%uFF41%uFF42%uFF43%uFF44%uFF45%uFF46%uFF47%uFF48%uFF49%uFF4A%uFF4B%uFF4C%uFF4D%uFF4E%uFF4F%uFF50%uFF51%uFF52%uFF53%uFF54%uFF55%uFF56%uFF57%uFF58%uFF59%uFF5A%uFF5B%uFF5C%uFF5D%uFF5E";
    
  var result = "";
  for ( var i = 0 ; i < text.length ; i ++ ) {
    var val = text.charAt(i) ;            
    var j = asciiTable.indexOf(val) * 6 ;        
    result += ( j > -1 ? unescape(big5Table.substring( j , j + 6 ) ) : val );    
  }
      
  return result;
 
}
function thanSetNumQ(num,setNumQ){ //比較數值，取最小值
	if(num>=setNumQ){
		return setNumQ;
	}else{
		return num;
	}
}
function checkGrammarChart(course_id,callBack){ //檢查文法表格圖片資料
	var imgNum=9;
	var imgArray=new Array();
	var checkImgArr=new Array();
	checkImgArr.push(getBaseFile()+course_id+"_grammar chart.png");
	for(var i=0;i<imgNum;i=i+1){
		checkImgArr.push(getBaseFile()+course_id+"_grammar chart"+(i+1)+".png");
	}
	for(var i=0;i<checkImgArr.length;i=i+1){
		var image = new Image;
		image.onload = function() {
			// At this point, there's no error.
			if ('naturalHeight' in this) {
				if (this.naturalHeight + this.naturalWidth === 0) {
					this.onerror();
					return;
				}
			} else if (this.width + this.height == 0) {
				this.onerror();
				return;
			}
			imgArray.push(this.src);
			if(imgArray.length === checkImgArr.length){
				addImg(imgArray);
			}
		};
		image.onerror = function() {
			//display error
			imgArray.push(null);
			if(imgArray.length === checkImgArr.length){
				addImg(imgArray);
			}
		};
		image.src = checkImgArr[i];
	}
	function addImg(img){
		for(var i=img.length;i>=0;i=i-1){
			if(img[i] === null){
				img.splice(i,1);
			}
		}
		callBack(img);
	}
}
function checkImgData(imgs,callBack){ //檢查一之多個圖片資料
	var checkImgArr=new Array();
	var imgArray=new Array();
	if(imgs.constructor === Array){
		checkImgArr=clone(imgs);	
	}else{
		checkImgArr.push(imgs);
	}
	for(var i=0;i<checkImgArr.length;i=i+1){
		var image = new Image;
		image.onload = function() {
			// At this point, there's no error.
			if ('naturalHeight' in this) {
				if (this.naturalHeight + this.naturalWidth === 0) {
					this.onerror();
					return;
				}
			} else if (this.width + this.height == 0) {
				this.onerror();
				return;
			}
			var obj = {};
			obj.src=this.src;
			obj.width=this.width;
			obj.height=this.height;
			imgArray.push(obj);
			if(imgArray.length === checkImgArr.length){
				addImg(imgArray);
			}
		};
		image.onerror = function() {
			//display error
			imgArray.push(null);
			if(imgArray.length === checkImgArr.length){
				addImg(imgArray);
			}
		};
		image.src = checkImgArr[i];
	}
	function addImg(img){
		if(img.length !== 1){
			for(var i=img.length;i>=0;i=i-1){
				if(img[i] === null){
					img.splice(i,1);
				}
			}
			callBack(img);
		}else{
			callBack(img[0]);
		}
	}
}
function processLineUtags(ques,ques_c){
	var t1=ques;
	var t2=ques_c;    
	var t11 = t1.split(ModuleSet.REPLACEBR);
	var strArray = new Array();
	for(var i=0;i<t11.length;i=i+1){
		if(t11[i].indexOf("<u>")!=-1 || t11[i].indexOf("___")!=-1){
			strArray.push(t11[i]+ModuleSet.REPLACEBR+"（"+t2+"）");
		}else{
			strArray.push(t11[i]);
		}
	}
	var tt="";
	for(var i=0;i<strArray.length;i=i+1){
		if(i==strArray.length-1){
			tt+=strArray[i];
		}else{
			tt+=strArray[i]+ModuleSet.REPLACEBR;
		}
	}
	return tt;
}
//----參考main/systemCheck.js-------
function browserTypeof(){
	var Sys = {} ;
	var sys = new systemCheck(window);
	Sys=sys.GetBrowserVer();
	sys=null;
	return Sys;
}
function isMobileBrowser(){ //利用 UserAgnet 判斷是否為行動裝置瀏覽網頁
    var isMobile = false;
    var sys = new systemCheck(window);
	isMobile = sys.isMobileBrowser();
	sys=null;
	return isMobile;
}
function moduleClass(module, obj){
	var t = "no-touch";
	if(isTouchDevice()){
		t = "touch";
	}
	obj.className = module + " " + t + " " + obj.className;
}
function changeClass(oldname, newname, obj){
	if(obj !== null){
		obj.className = obj.className.replace(oldname, newname);
	}
}
function addClass(name, obj){
	if(obj !== null){
		obj.className = obj.className + " " + name;
	}
}
//是否可觸控
function isTouchDevice() {
    return !!('ontouchstart' in window) || (!!('onmsgesturechange' in window) && !!window.navigator.maxTouchPoints);
}
//小數點第N位四捨五入
function formatFloat(num, pos){
  var size = Math.pow(10, pos);
  return Math.round(num * size) / size;
}
//上層是否為iframe
function inIframe() {
    try {
        return window.self !== window.top;
    } catch (e) {
        return true;
    }
}
//----參考main/Javascript_eXtension_library.js-------
function getElementTop(element){return element.getElementTop();}
function getElementLeft(element){return element.getElementLeft();}
function arrayClear(array){return array.clearNull();}
function prev(elem) {return elem.prev();}
function next(elem) {return elem.next();}
function first(elem) {return elem.first();}
function last(elem) {return elem.last();}
function _parent(elem, num) {return elem._parent(num);}
function trim(str){return str.trim();}
function clearTable(table){table.clearTable();}