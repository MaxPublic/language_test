﻿//V2.0.0 20191111
var nanobar;
var timer;
var counter;
var audio;
var examData = {"at_accnt":"", "mapid":0 ,"Answer":[], "pass":false };
var systemData = { "examid": "", "nowID": 0, "playing": 0, "url": "/ws2016.asmx", "rid": "", "mode": 1 };
var options = {
	bg: "#F3A265"
};
function initLoad(){
	var st = "";
	st += '<div class="logo clearfix"><img class="image" src="images/gjunlogo.svg"></div>';
	st += '<div id="counter"></div>';
	st += '<div class="is-hide"><div id="container" class=""></div></div>';
	document.body.innerHTML = st;
	var jsonData = ["main/jquery.js","main/method.js","main/nanobar.min.js","main/base64.js","main/jquery.lazyload.min.js","main/main.js","css/main.css","css/standardize.css","css/start.css"];
	_import(jsonData,document,initData);
}
function initData(){
	//getQueryString();
	//getLocalData();
	getTestData();
}
function init(){
	//nanobar = new Nanobar(options);
	//nanobar.go(30);
	disableSelection(document.body);
	if(systemData.mode == 1){
		go(goIntro,300,false);
	}else if(systemData.mode == 2){
		go(goExam,300,true);
	}else if(systemData.mode == 3){
		go(goResult,300,true);
	}
	//go(goResult,300,true);
}
function goIntro(){
	systemData.nowID = 0;
	var container = document.getElementById("container");
	var st = "";
	st += '<div class="subwrapper"><div class="all clearfix"><p class="title">本測驗為：'+data.DocumentElement.Info.ExamTitle+'</p><span class="point-text">';
	if(data.DocumentElement.Info.ExamType != "ab"){
		st += '共分兩個部份，Reading、Listening，';
	}
	st += '共'+data.DocumentElement.Exam.length+'題，作答時間約需'+data.DocumentElement.Info.ExamTime+'分鐘。';
	if(data.DocumentElement.Info.ExamType != "ab"){
		st += '本測驗含聽力題，開始測驗前，請確認您的喇叭或耳機是否正常。<br>';
	}
	st += '作答過程中如確認交卷，即進行計分；如不交卷，請直接於右上角關閉網頁即可。</span></div>';
	st += '<div class="btnnext clearfix"><button class="_button" onClick="start(this)">開始測驗</button></div></div>';
	container.innerHTML = st;
}
function start(o){
	o.disabled='true';
	go(goExam,300,true);
}
function go(f,s,c){
	var exam = data.DocumentElement.Exam[systemData.nowID];
	var groupObj = getObjects(data.DocumentElement.Exam,"GroupID",exam.GroupID);
	var moduleName = getClearValue(exam.ModuleName);
	var container = document.getElementById("container");
	nanobar = new Nanobar(options);
	changeClass("is-show", "is-hide", container.parentNode);
	
	timer = window.setInterval(
	function(){
		if(c == true){
			var css="";
			if(f == goResult){
				css = "result";
			}else{
				if(moduleName=="ReadingMCQ"){
					if(groupObj.length>1){
						css = "reading_4sel_n_q";
					}else{
						css = "reading_4sel_pic";
					}
				}else if(moduleName=="ListeningMCQ"){
					css = "listening_4sel_n_q";
				}			
			}
			var stylesheet = document.styleSheets[document.styleSheets.length-1];
			var oldcss = stylesheet.href.split('/')[stylesheet.href.split('/').length-2]+"/"+stylesheet.href.split('/')[stylesheet.href.split('/').length-1];
			var newcss = 'css/'+css+'.css';
			if(oldcss !== newcss){
				$('link[href="'+oldcss+'"]').remove();
				var jsonData = [newcss];
				_import(jsonData,document,show);
				//replacejscssfile(oldcss,newcss,'css');
			}else{
				show();
			}
		}else{
			show();
		}
		window.clearInterval(timer);
	}, s);
	function show(){
		container.innerHTML = "";
		changeClass("is-hide", "is-show", container.parentNode);
		if(f){f();}
	}
	nanobar.go(100);
	/*
	timer = window.setInterval(
	function(){
		if(c == true){
			var src=$(document).find('link')[document.styleSheets.length - 1].href;replacejscssfile(src.split('/')[src.split('/').length-2]+"/"+src.split('/')[src.split('/').length-1],'css/'+css+'.css','css');
		}
		container.innerHTML = "";changeClass("is-hide", "is-show", container.parentNode);window.clearInterval(timer);if(f){f();}
	}, s);
	*/
}
function jumpExam(n){
	systemData.nowID = n;
	showBox();
	go(goExam,300,true);
}
function startTimer(duration, display,callback) {
    var timer = duration, minutes, seconds;
    counter = setInterval(function () {
        if (--timer < 0) {
            //timer = duration;
			window.clearInterval(counter);
			if(callback){
				callback();
			}
			
        }else{
			minutes = parseInt(timer / 60, 10);
			seconds = parseInt(timer % 60, 10);

			minutes = minutes < 10 ? "0" + minutes : minutes;
			seconds = seconds < 10 ? "0" + seconds : seconds;
			if(display){
				display.innerText = minutes + ":" + seconds;
				//display.text(minutes + ":" + seconds);
			}
		}
    }, 1000);
}
function padLeft(str, len) {
    str = '' + str;
    if (str.length >= len) {
        return str;
    } else {
        return padLeft("0" + str, len);
    }
}
function timeOut() {
	var box = document.getElementById("box");
	box.style.display = "none";
	var st = "";
	st += '<div class="tableexit clearfix">';
	st += '<div class="clearfix">';
	st += '<p class="text">測驗時間結束</p>';
	st += '</div>';
	st += '<div class="tablebtn _flexcenter">';
	st += '<button class="_button _button-2" onClick="showResult();">確定</button>';
	st += '</div>';
	st += '</div>';
	box.innerHTML = st;
	showBox();
	
}
function goExam(){
	var exam = data.DocumentElement.Exam[systemData.nowID];
	var groupObj = getObjects(data.DocumentElement.Exam,"GroupID",exam.GroupID);
	var moduleName = getClearValue(exam.ModuleName);
	var name = "";
	if(moduleName=="ReadingMCQ"){
		name = "Reading";
	}else if(moduleName=="ListeningMCQ"){
		name = "Listening";
	}
	clearAudio();
	var container = document.getElementById("container");
	var st = "";
	st += '<div class="subwrapper">';
	st += '<div class="q_num clearfix"><p class="num1">Part '+exam.Part+"_"+name+'</p></div>';
	st += '<div class="top clearfix"><p class="inx_extxt">'+getValue(exam.Hint)+'</p><div class="point"><div class="bluepoint"></div></div></div>';
	st += '<div class="questionall">';

	if(groupObj.length>1 || moduleName=="ListeningMCQ"){
		st += '<div class="qleft clearfix">';
		if(moduleName=="ListeningMCQ"){
			st += '<div class="container container-1">';
			st += '<button id="play" onClick="play();"><img class="image" src="images/btn_play.svg"></button><button id="pause" onClick="pause();" style="display: none;"><img class="image" src="images/btn_pause.svg"></button>';
			st += '</div>';			
		}
		st += '<div class="container clearfix">';
		if(getValue(groupObj[0].GroupQuestion).trim()!==""){
			st += '<div class="text text-1 textinner">'+getValue(groupObj[0].GroupQuestion)+'</div>';
		}
		if(getValue(groupObj[0].GroupPicture).trim()!== ""){
			st += '<div class="text imageinner loader"><img class="lazy" data-original="'+getMaterialFile()+groupObj[0].GroupPicture+'"/></div>';
		}
		st += '</div>';
		st += '</div>';
	}
	if(groupObj.length>1 || moduleName=="ListeningMCQ"){
		st += '<div class="qright clearfix">';
	}else{
		st += '<div class="qleft clearfix">';
	}
	st += '<p class="num1">Q'+(systemData.nowID+1);
	if(groupObj.length>1){
		st += ' ('+groupObj[0].ID+'~'+groupObj[groupObj.length-1].ID+')</p>';
	}else{
		st += '</p>';
	}
	if(moduleName=="ReadingMCQ"){
		if(groupObj.length>1){
			st += '<p class="text text-3">';
		}else{
			st += '<p class="text text-1">';
		}
	}else if(moduleName="ListeningMCQ"){
		st += '<p class="text text-2">';
	}
	var q = "&nbsp;";
	if(getValue(exam.Question)!==""){
		q = getValue(exam.Question);
	}
	st += q+'</p>';
	if(groupObj.length<=1 && getValue(groupObj[0].GroupPicture).trim()!== ""){
		st += '<div class="container clearfix">';
		if(getValue(groupObj[0].GroupPicture).trim()!== ""){
			st += '<div class="text imageinner loader"><img class="lazy" data-original="'+getMaterialFile()+groupObj[0].GroupPicture+'"/></div>';
		}
		st += '</div>';
	}
	if(groupObj.length>1 || moduleName=="ListeningMCQ"){
		st += initOptions(exam);
		if(systemData.mode == 1){
			st += initBtn();
		}
	}
	st += '</div>';
	if(moduleName=="ReadingMCQ"){
		if(groupObj.length<=1){
			st += '<div class="qright">';
			st += initOptions(exam);
			if(systemData.mode == 1){
				st += initBtn();
			}
			st += '</div>';
		}
	}
	/*
	if(systemData.mode == 1){
		if(systemData.nowID<data.DocumentElement.Exam.length-1){
			st += '<div class="btnnext clearfix"><button class="_button" onClick="nextExam(this);">下一題</button></div>';
		}else{
			st += '<div class="btnnext clearfix"><button class="_button" onClick="showResult();">交卷</button></div>';
		}	
	}
	*/
	if(systemData.mode == 1){
		if(document.getElementsByClassName("logo")[0] && !document.getElementById("exit")){
			var logo = document.getElementsByClassName("logo")[0];
			logo.innerHTML += '<button id="exit" class="_button" onClick="showBox();">交卷</button>';
		}
	}
	st += '</div>';
	st += '</div>';
	if(systemData.mode == 2){
		var userans;
		if(getObjects(examData.Answer,"ID",exam.ID)[0]){
			userans = getAnsWord(exam, decodeAns(getObjects(examData.Answer,"ID",exam.ID)[0].Answer))
		}else{
			userans = "未作答";
		}
		st += '<div class="ans">';
		st += '<div class="btnendtxt clearfix">';
		st += '<div class="text analysis"><p>您的回答：'+userans+'　　<span>解答：'+getAnsWord(exam, exam.Answer)+'</span></p><p>解析：</p></div>';
		st += '<p class="text text-5">'+getValue(exam.Explain)+'<br></p>';
		st += '</div>';
		st += '<div class="btnendall">';
		st += '<div class="btnend clearfix">';
		st += '<button class="_button _button-6" onClick="showBox();">答題記錄</button>';
		if(systemData.nowID==0){
			st += '<button disabled="true" class="_button _button-7 _disable">上一題</button>';
		}else{
			st += '<button class="_button _button-7 _enable" onClick="preExam(this);">上一題</button>';
		}
		if(systemData.nowID==data.DocumentElement.Exam.length-1){
			st += '<button disabled="true" class="_button _button-8 _disable">下一題</button>';
		}else{
			st += '<button class="_button _button-8 _enable" onClick="nextExam(this);">下一題</button>';
		}
		st += '</div>';
		st += '</div>';
		st += '</div>';
		document.body.style.backgroundColor = "#f4e8d7"; 
	}
	container.innerHTML = st;
	if(systemData.mode == 1){
		if(!document.querySelector('#counter>span:nth-child(1)')){
			document.querySelector('#counter').innerHTML = '<span>'+padLeft(data.DocumentElement.Info.ExamTime,2)+':00</span><span>作答剩餘時間：</span>';
			var sec = 60 * parseInt(data.DocumentElement.Info.ExamTime);
			//var sec = 5;
			var display = document.querySelector('#counter>span:nth-child(1)');
			startTimer(sec, display,timeOut);
		}
		container.innerHTML = container.innerHTML + initBox(systemData.mode);	
		window.setInterval(function(){
				if(document.getElementById("box")){
					var h = $(document).height();
					if(scrollBars().vertical){
						h = document.body.scrollHeight;
					}
					document.getElementById("box").style.height = h+'px';
				}
			}, 300);
	}else if(systemData.mode == 2){
		container.innerHTML = container.innerHTML + initBox(systemData.mode);	
		window.setInterval(function(){
				if(document.getElementById("box")){
					var h = $(document).height();
					if(scrollBars().vertical){
						h = document.body.scrollHeight;
					}
					document.getElementById("box").style.height = h+'px';
				}
			}, 300);
	}
	lazyLoad();
}
function initBtn(){
	var st ='';
	st += '<div class="btnnext clearfix">';
	st += '<div class="btn3 clearfix">';
	if(systemData.nowID==0){
		st += '<button disabled="true" class="_button _left _disable">上一題</button>';
	}else{
		st += '<button class="_button _left _enable" onClick="preExam(this);">上一題</button>';
	}
	if(systemData.nowID==data.DocumentElement.Exam.length-1){
		st += '<button disabled="true" class="_button _right _disable">下一題</button>';
	}else{
		st += '<button class="_button _right _enable" onClick="nextExam(this);">下一題</button>';
	}
	st += '</div>';
	st += '</div>';
	return st;
}	
function lazyLoad(){
	$("img.lazy").lazyload();
	$("img.lazy").lazyload({
		effect : "fadeIn",
		threshold : 50
	});
}
function scrollBars(){
	var body= $('body')[0]
	return {
		vertical:body.scrollHeight> Math.max(window.innerHeight, body.clientHeight),
		horizontal:body.scrollWidth>body.clientWidth
	}
}
function showResult(){
	window.clearInterval(counter);
	saveRec();
	if(document.querySelector('#counter>span:nth-child(1)')){
		document.querySelector('#counter').innerHTML = '';
	}
	if(document.getElementById('exit')){
		var exit = document.getElementById('exit');
		exit.style.display = "none";
	}
	if(document.getElementById('box')){
		var box = document.getElementById('box');
		box.style.display = "none";		
	}
	clearAudio();
	go(goResult,300,true);
}
function getType(){
	var type = [];
	var t = "";
	for (var i = 0; i < data.DocumentElement.Exam.length; i = i + 1) {
		var exam = data.DocumentElement.Exam[i];
		if(exam){
			if(exam.Type){
				if(exam.Type !== t){
					t = exam.Type;
					run();
				}
			}			
		}
	}
	function run(){
		var io = 1;
		if(type.length > 0){
			for (var i = 0; i < type.length; i = i + 1) {
				if(t == type[i]){		
					io = 0;
					break;
				}
			}			
		}
		if(io == 1){
			type.push(t);			
		}
	}
	//alert(JSON.stringify(type));
	return type;
}
function getRightNum(n){
	var result = 0;
	for (var i = 0; i < examData.Answer.length; i = i + 1) {
		var io = false;
		var ans = getObjects(data.DocumentElement.Exam,"ID",examData.Answer[i].ID)[0].Answer;
		var userans = decodeAns(getObjects(examData.Answer,"ID",examData.Answer[i].ID)[0].Answer);
		if(n){
			var type = getObjects(data.DocumentElement.Exam,"ID",examData.Answer[i].ID)[0].Type;
			if(n == type){
				io = true;
			}
		}else{
			io = true;
		}
		if(io == true){
			if(userans == ans){
				result ++;
			}
		}		
	}
	return result;
}
function goResult(){
	var scoreData = {"title":["測驗能力","測驗題數","答對","答錯","答對率"],"css":["point-text","titlenum","titleright","titlewrong","titlepercent"]};
	var container = document.getElementById("container");
	defaultExam();
	var st = "";
	st += '<div class="subwrapper">';
	st += '<div class="q_num clearfix"><p class="num1">測驗結果：</p></div>';
	st += '<div class="question_sound">';
	st += '<div class="table1">';
		for (var i = 0; i < scoreData.title.length; i = i + 1) {
			st += '<div class="list1 list1-'+(i+1)+' clearfix">';
			st += '<div class="titletxt titletxt-'+(i+1)+'">'+scoreData.title[i]+'</div>';
			st += '<div class="leftsel leftsel-'+(i+1)+' clearfix">';
			st += initScore(i,scoreData.css[i]);
			st += '</div>';
			st += '</div>';
		}
	st += '</div>';
	st += '</div>';
	//if(systemData.mode != 3){
		st += '<div class="btnnext clearfix">';
		st += '<button class="_button analysisbtn" onClick="analysis(this);">看解析</button>';
		st += '</div>';		
	//}
	st += '</div>';
	container.innerHTML = st;
	
	function initScore(n,css){
		var st='';
		var obj = getType();
		for (var i = 0; i < obj.length; i = i + 1) {
			var length = getObjects(data.DocumentElement.Exam,"Type",obj[i]).length;
			var right = getRightNum(obj[i]);
			var wrong = length - right;
			
			var percent = Math.ceil((right/length)*100);
			if(n==0){
				st += '<span class="point-text '+css+'-'+(i+1)+'">'+obj[i]+'</span>';
			}else if(n==1){
				st += '<span class="point-text '+css+'-'+(i+1)+'">'+length+'</span>';
			}else if(n==2){
				st += '<span class="point-text '+css+'-'+(i+1)+'">'+right+'</span>';
			}else if(n==3){
				st += '<span class="point-text '+css+'-'+(i+1)+'">'+wrong+'</span>';
			}else if(n==4){
				st += '<span class="point-text '+css+'-'+(i+1)+'">'+percent+' %</span>';
			}
		}
		return st;
	}
}

function play(){
	if(systemData.playing == 0){
		var exam = data.DocumentElement.Exam[systemData.nowID];
		var groupexam = getObjects(data.DocumentElement.Exam,"GroupID",exam.GroupID);
		var snd = exam.GroupAudio;
		if(groupexam.length > 1){
			snd = groupexam[0].GroupAudio;
			
		}
		if(snd!==""){
			PlaySound(snd);
		}		
	}else{
		audio.play();
	}
	SoundBtn(1);
}
function pause(){
	if(audio){
		systemData.playing = 0;
		audio.pause();
		SoundBtn(0);
	}
}
function stop(){
	if(audio){
		systemData.playing = 0;
		audio.pause();
		if(audio.currentTime){
			audio.currentTime = 0;
		}
	}
}
function PlaySound(snd){
	if(!audio){
		audio = document.createElement("audio");
	}
	audio.src = getMaterialFile() + snd;
	audio.play();
	audio.addEventListener("canplay",SoundCanPlay);	
}
function SoundCanPlay(){
	//audio.play();
	systemData.playing = 1;
	audio.addEventListener('ended', SoundEnded, false);	
	audio.removeEventListener('canplay', SoundCanPlay ,false);
}
function SoundEnded(){
	systemData.playing = 0;
	SoundBtn(0);
	audio.removeEventListener('ended', SoundEnded, false);
}
function SoundBtn(n){
	if(n == 0){
		document.getElementById("play").style.display = "";
		document.getElementById("pause").style.display = "none";			
	}else if(n == 1){
		document.getElementById("play").style.display = "none";
		document.getElementById("pause").style.display = "";		
	}
}
function showBox(){
	var box = document.getElementById("box");
	if(box.style.display == "none"){
		box.style.display = "block";
	}else{
		box.style.display = "none";
	}
	
}
function initBox(n){
	var st = "";
	if(n == 1){
		st += '<div id="box" class="windows" style="display: none;">';
		st += '<div class="tableexit clearfix">';
		st += '<div class="close" onClick="showBox();"><img class="image" src="images/btn_close.svg"></div>';
		st += '<div class="clearfix">';
		st += '<p class="text">您確定要交卷嗎？</p>';
		st += '</div>';
		st += '<div class="tablebtn clearfix">';
		st += '<button class="_button _button-2" onClick="showResult();">確定</button>';
		st += '<button class="_button _button-3" onClick="showBox();">取消</button>';
		st += '</div>';
		st += '</div>';
		st += '</div>';		
	}else if(n == 2){
		st += '<div id="box" class="windows" style="display: none;">';
		st += '<div class="tablemenu clearfix">';
		st += '<div class="close" onClick="showBox();"><img class="image" src="images/btn_close.svg"></div>';
		for (var i = 0; i < data.DocumentElement.Exam.length; i = i + 1) {
			var c ='';
			if(checkAns(data.DocumentElement.Exam[i].ID) == false){
				c = " red";
			}
			st += '<button class="_btn'+c+'" onClick="jumpExam('+i+');">'+(i+1)+'</button>';
		}
		st += '</div>';
		st += '</div>';
	}

	return st;
}
/*
function initBox(){
	var st = "";
	st += '<div id="box" class="windows" style="display: none;">';
	st += '<div class="tablemenu clearfix">';
	st += '<div class="close" onClick="showBox();"><img class="image" src="images/btn_close.svg"></div>';
	for (var i = 0; i < data.DocumentElement.Exam.length; i = i + 1) {
		var c ='';
		
		if(checkAns(data.DocumentElement.Exam[i].ID) == false){
			c = " red";
		}
		st += '<button class="_btn'+c+'" onClick="jumpExam('+i+');">'+(i+1)+'</button>';
	}
	st += '</div>';
	st += '</div>';
	return st;
}
*/
function initOptions(exam){
	var st="";
	var userans="";
	//if(systemData.mode == 2){
	if(getObjects(examData.Answer,"ID",exam.ID)[0]){
		userans = decodeAns(getObjects(examData.Answer,"ID",exam.ID)[0].Answer);
	}
	//}
	if(exam.Picture && exam.Picture.trim()!==""){
		st += '<div class="container clearfix"><div class="text loader"><img class="lazy" data-original="'+getMaterialFile()+exam.Picture+'"></div></div>';
	}
	for (var i = 1; i <= 4; i = i + 1) {
		var op = exam["Item_"+i];
		var io = false;
		var css = "_button-2";
		var t="";
		var enable = "";
		var click='selected('+i+',this)';
		if(op!==""){
			if(userans == op){
				io = true;
				if(systemData.mode == 1){					
					css = "_button-4";
				}else if(systemData.mode == 2){					
					if(checkAns(exam.ID)){
						css = "_button-1";
						t = '<div class="ans_icon ans_icon-1"><img class="image image-2" src="images/icon_yes.svg"></div>';
					}else{
						t = '<div class="ans_icon ans_icon-2"><img class="image image-3" src="images/icon_wrong.svg"></div>';
					}

				}

			}
			if(systemData.mode == 2){		
				click="";
				enable = "disabled='true' ";
			}
			if(io == false){
				var rcss = "_button-3";
				if(systemData.mode == 2){
					if(exam.Answer == op){
						rcss = "_button-1";
					}
				}
				st += '<div class="selection"><div class="cover clearfix"><button '+enable+'class="_button _boxshadow '+rcss+'" onClick="'+click+'"><span>'+numToWord(i)+'.</span>'+op+'</button></div></div>';

			}else if(io == true){
				st += '<div class="selection"><div class="cover clearfix"><button '+enable+'class="_button _boxshadow '+css+'" onClick="'+click+'"><span>'+numToWord(i)+'.</span>'+op+'</button></div>'+t+'</div>';
			}
		}
	}
	return st;
}
function numToWord(n){
	if(n == 1){
		return "A";
	}else if(n == 2){
		return "B";
	}else if(n == 3){
		return "C";
	}else if(n == 4){
		return "D";
	}
}
function getAnsWord(exam, ans){
	var result;
	for (var i = 1; i <= 4; i = i + 1) {
		var op = exam["Item_"+i];
		if(op!==""){
			if(op == ans){
				result = numToWord(i);
			}
		}
	}
	return result;
}
function defaultOption(o){
	var exam = data.DocumentElement.Exam[systemData.nowID];
	for (var i = 0; i < o.parentNode.parentNode.parentNode.getElementsByTagName("button").length; i = i + 1) {
		var btn = o.parentNode.parentNode.parentNode.getElementsByTagName("button")[i];
		if(btn.className.indexOf("_button-4")!==-1){
			btn.className = btn.className.replace("_button-4","_button-3");
		}
	}	
}
function selected(n,o){
	var exam = data.DocumentElement.Exam[systemData.nowID];
	defaultOption(o);
	o.className = o.className.replace("_button-3","_button-4");
	saveAnsData(exam.ID,exam["Item_"+n]);
}
function decodeAns(str){
	var result;
	//result = str.replace(/-/g, '+').replace(/_/g, '/');
	result = str.replace(/_/g, '/');
	return result;
}
function checkAns(id){
	var result = false;
	if(getObjects(examData.Answer,"ID",id)[0]){
		var userans = getObjects(examData.Answer,"ID",id)[0].Answer;
		userans = decodeAns(userans);
		var ans = getObjects(data.DocumentElement.Exam,"ID",id)[0].Answer;
		//console.log(userans+"@@"+ans)
		if(userans == ans){
			result = true;
		}
	}
	return result;
}
function saveAnsData(id,ans){
	if(getObjects(examData.Answer,"ID",id)[0]){
		getObjects(examData.Answer,"ID",id)[0].Answer = ans;
	}else{
		var obj = new Object();
		obj.ID = id;
		obj.Answer = ans;
		examData.Answer.push(obj);
	}
	//console.log(JSON.stringify(examData))
	//alert(JSON.stringify(examData.Answer));
}
function deleteAnsData(id){
	if(getObjects(examData.Answer,"ID",id)[0]){
		var index = findIndexByKeyValue(examData.Answer, "ID", id);
		examData.Answer.splice(index, 1);
		//alert(JSON.stringify(examData.Answer));
	}
}
function saveScoreData(id,score){
	if(getObjects(examData.Answer,"ID",id)[0]){
		getObjects(examData.Answer,"ID",id)[0].Score = score;
	}else{
		var obj = new Object();
		var exam = getObjects(doc.Exam,"ID",id)[0];
		obj.ID = id;
		obj.Score = score;
		examData.Answer.push(obj);
	}
	//console.log(JSON.stringify(examData))
	//alert(JSON.stringify(examData.Answer));
}
function reExam(){
	clearAudio();
	examData.answers = [];
	document.getElementsByClassName("againbtn")[0].style.display = "none";
	deleteAnsData((systemData.nowID+1));
}
function preExam(o){
	o.disabled='true';
	systemData.nowID--;
	go(goExam,300,true);
}
function nextExam(o){
	o.disabled='true';
	systemData.nowID++;
	go(goExam,300,true);
}
function analysis(o){
	o.disabled='true';
	systemData.mode = 2;
	go(goExam,300,true);
}
function defaultExam(){
	systemData.nowID = 0;
}
function clearAudio(){
	if(audio){
		stop();
		audio.src = "";
	}
}
function isPass(){
	var result = true;
	if(data.DocumentElement.Info.ExamType == "ab"){
		var length = data.DocumentElement.Exam.length;
		var right = getRightNum();
		var percent = Math.ceil((right/length)*100);
		if(percent < 60){
			result = false;
		}
	}else{
		var obj = getType();
		for (var i = 0; i < obj.length; i = i + 1) {
			var length = getObjects(data.DocumentElement.Exam,"Type",obj[i]).length;
			var right = getRightNum(obj[i]);
			var wrong = length - right;
			var percent = Math.ceil((right/length)*100);
			if(percent < 80){
				result = false;
				break;
			}
		}
	}
	return result;
}
function saveRec(){ //儲存考試紀錄
	var jsonData = JSON.parse(JSON.stringify(examData));
	jsonData.Answer = Base64.encode(Base64EncodeUrl(JSON.stringify(jsonData.Answer)));
	jsonData.pass = isPass();
	var strURL = systemData.url + '/STU_TEST_SAVE'; 
	$.ajax({
		type: 'POST',
		url: strURL,
		contentType: "application/json; charset=utf-8",
		dataType: 'json',         
	    //data: jsonData,
		data: JSON.stringify(jsonData),
		success: function (msg) { 
			if(msg.result == "0"){
				alert('存檔失敗，請洽客服');
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			alert("儲存考試紀錄發生問題 xhr:"+xhr+" thrownError:"+thrownError);
			//alert(xhr.status);
			//alert(thrownError);
		}
	});
}

function loadRec(){
	var strURL = systemData.url+'/STU_TEST_GET';            
	$.ajax({
		type: 'POST',
		url: strURL, 
		contentType: "application/json; charset=utf-8",
		dataType: 'json',            
		data: '{ recid: '+systemData.rid+' }', 
		success: function (obj) { 
			//alert(JSON.stringify(obj.d));
			examData.Answer = JSON.parse(Base64.decode(Base64DecodeUrl(obj.d)));
			getData();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			//alert(xhr.status);
			//alert(thrownError);
		}
	});
}

function getRec(){
	getData();
}
function login(at_accnt, mapid){
	var jsonData = JSON.parse(JSON.stringify(examData));
	jsonData.Answer = Base64.encode(Base64EncodeUrl(JSON.stringify(jsonData.Answer)));
	var strURL = systemData.url+'/Check_TEST_LIST';
	$.ajax({
		type: 'POST',
		url: strURL,
	    //dataType: 'xml',
		contentType: "application/json; charset=utf-8",
		dataType: 'json',   
	    //data: { at_accnt: at_accnt, mapid: mapid },
		data: "{ at_accnt: '" + at_accnt + "', mapid:" + mapid + " }",
		success: function (oXml) {
		    //var n = $('string', oXml).text();
		    var n = oXml.d;
			if(n == 1){
				examData.at_accnt = at_accnt;
				examData.mapid = mapid;
				getData();				
			}else if(n == 0){
				document.body.innerHTML = "例外錯誤";
			}else if(n == 2){
				document.body.innerHTML = "沒有考試資格或找不到考卷";
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			//alert(xhr.status);
			//alert(thrownError);
		}
	});		
}
function getQueryString(){
	var acc = getParameterByName("acc");
	var mid = getParameterByName("mid");
	var rid = getParameterByName("rid");
	var cid = getParameterByName("cid");
	var mode = getParameterByName("mode");
	if(cid){
		systemData.examid = cid;
		if(acc && mid){
			login(acc, parseInt(mid));
			//alert(getParameterByName("s")+"@"+getParameterByName("t"))
		}else if(rid && mode){
			systemData.rid = parseInt(rid);
			systemData.mode = mode;
			loadRec();
			//getData();
		}else{
			document.body.innerHTML = "找不到考卷資料";
		}
	}else{
		document.body.innerHTML = "找不到考卷資料";
	}
}
function getData(){
	var jsonData = ["data/"+systemData.examid+".js"];
	_import(jsonData,document,init);
}
function getLocalData(){
	var jsonData = ["data/data.js"];
	_import(jsonData,document,init);
}
function getTestData(){
	var name = getParameterByName("class");
	console.log(name)
	var jsonData = ["data/"+name+".js"];
	_import(jsonData,document,init);
}
function disableSelection(target){//無法選取反白
    //For IE This code will work
    if (typeof target.onselectstart!="undefined") 
		target.onselectstart=function(){return false}
    
    //For Firefox This code will work
    else if (typeof target.style.MozUserSelect!="undefined") 
		target.style.MozUserSelect="none"
    
    //All other  (ie: Opera) This code will work
    else 
		target.onmousedown=function(){return false}
		target.style.cursor = "default"
}
function createjscssfile(filename, filetype){
    if (filetype=="js"){ //if filename is a external JavaScript file
        var fileref=document.createElement('script')
        fileref.setAttribute("type","text/javascript")
        fileref.setAttribute("src", filename)
    }
    else if (filetype=="css"){ //if filename is an external CSS file
        var fileref=document.createElement("link")
        fileref.setAttribute("rel", "stylesheet")
        fileref.setAttribute("type", "text/css")
        fileref.setAttribute("href", filename)
    }
    return fileref
}
function removejscssfile(filename, filetype){
    var targetelement=(filetype=="js")? "script" : (filetype=="css")? "link" : "none" //determine element type to create nodelist from
    var targetattr=(filetype=="js")? "src" : (filetype=="css")? "href" : "none" //determine corresponding attribute to test for
    var allsuspects=document.getElementsByTagName(targetelement)
    for (var i=allsuspects.length; i>=0; i--){ //search backwards within nodelist for matching elements to remove
    if (allsuspects[i] && allsuspects[i].getAttribute(targetattr)!=null && allsuspects[i].getAttribute(targetattr).indexOf(filename)!=-1)
        allsuspects[i].parentNode.removeChild(allsuspects[i]) //remove element by calling parentNode.removeChild()
    }
}
function replacejscssfile(oldfilename, newfilename, filetype){
    var targetelement=(filetype=="js")? "script" : (filetype=="css")? "link" : "none" //determine element type to create nodelist using
    var targetattr=(filetype=="js")? "src" : (filetype=="css")? "href" : "none" //determine corresponding attribute to test for
    var allsuspects=document.getElementsByTagName(targetelement)
    for (var i=allsuspects.length; i>=0; i--){ //search backwards within nodelist for matching elements to remove
        if (allsuspects[i] && allsuspects[i].getAttribute(targetattr)!=null && allsuspects[i].getAttribute(targetattr).indexOf(oldfilename)!=-1){
            var newelement=createjscssfile(newfilename, filetype)
            allsuspects[i].parentNode.replaceChild(newelement, allsuspects[i])
        }
    }
}
function _import(file,document,callback) { //執行加載多個外部JS文件或外部CSS文件
    function loadNext() {
        var done = false;
        var head = document.getElementsByTagName('head')[0];
		var fileName = file.shift();
		var subFileName = getSubFileName(fileName);
		var createElement;
		
		switch(subFileName){
			case ".js":
		        var createElement = document.createElement('script');
		        createElement.type = 'text/javascript';
		        createElement.src = fileName; // grab next script off front of array
		        break;
			case ".css":
		        var createElement = document.createElement('link');
			    createElement.setAttribute('rel', 'stylesheet');
			    createElement.setAttribute('type', 'text/css');
				createElement.setAttribute('href',fileName);  
		        //createElement.href = fileName; // grab next script off front of array
				//alert(createElement.href);
		        break;
		}
        createElement.onreadystatechange = function () {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                loaded();
            }
        }
        createElement.onload = loaded;
		head.appendChild(createElement);
        function loaded() {
            // check done variable to make sure we aren't getting notified more than once on the same script
            if (!done) {
                createElement.onreadystatechange = createElement.onload = null;   // kill memory leak in IE
                done = true;
				
                if (file.length != 0) {
                    loadNext();
                }else{
					callback();
				}
            }
        }
    }
    loadNext();
}
function getSubFileName(fileName){
	var subFileIndex = fileName.lastIndexOf(".");
	var subFile = fileName.slice(subFileIndex,fileName.length);	
	return subFile;
}
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}